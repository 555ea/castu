angular
    .module( 'MainApp' )
    .directive( 'stupidityForm', stupidityForm );

function stupidityForm($http,
                       StupiditySettingsFactory, GoogleMapsFactory, ExifFactory, Upload, StupidityFactory, UsersFactory,
                       hosts) {
    return {
        restrict: 'E',
        scope: {
            stupidities: '=',
        },
        templateUrl: hosts.getStatic() + '/js/Directives/StupidityForm/StupidityForm.html',
        link: function ( scope, elem, attrs ) {
            scope.file = null;

            scope.uploadPossible = false;
            scope.showUploadButton = true;
            scope.isModerator = false;

            // functions
            scope.fileSelected = fileSelected;
            scope.submitForm = submitForm;
            scope.cancelFileUpload = cancelFileUpload;
            scope.getUser = getUser();

            activate();

            function activate(){
                scope.stupidityLevels = StupiditySettingsFactory.getStupidityLevels();
                scope.stupidityCategoryLists = StupiditySettingsFactory.getStupidityCategoryLists();
                GoogleMapsFactory.subscribeOnMapLoad().then( function () {
                    scope.uploadPossible = true;
                } );
                initStupidity();
            }
            
            //@scope
            function getUser() {
                return UsersFactory.getUser();
            }

            // @scope
            function fileSelected() {
                scope.showUploadButton = false;
                ExifFactory.getExifPosition( scope.file ).then( function ( position ) {
                    GoogleMapsFactory.changePreviewMarker( position );
                } ).catch( function () {
                    GoogleMapsFactory.changePreviewMarker();
                } );
                Upload.resize( scope.file, 600 ).then( function ( f ) {
                    watermark( [f] )
                        .image( watermark.text.lowerRight( 'tupizm.com', '24px serif', '#fff', 0.5 ) )
                        .then( function ( img ) {
                            scope.file = Upload.dataUrltoBlob( getBase64Image( img ), 'test' );
                        } );
                } )
            }

            // @scope
            function submitForm( form ) {
                if ( form.$valid ) {
                    var pos = GoogleMapsFactory.getPreviewMarkerPosition();
                    var categories = [];
                    Upload.base64DataUrl( scope.file ).then( function ( base64 ) {
                        var stupidity = {
                            severity: scope.stupidity.severity,
                            category: scope.stupidity.category,
                            comment: scope.stupidity.comment,
                            location: [pos.lng(), pos.lat()],
                            image: base64
                        };
                        initStupidity();
                        StupidityFactory.create(stupidity).then(function(){
                            linkWithMapAndSocials(stupidity);
                        })
                    } );
                    scope.cancelFileUpload();
                }
            }

            // @scope
            function cancelFileUpload() {
                GoogleMapsFactory.disablePreviewMarker();
                scope.showUploadButton = true;
                delete scope.file;
            }

            function linkWithMapAndSocials( stupidity ) {
                GoogleMapsFactory.addMarker( stupidity );
            }

            function getBase64Image( img ) {
                // Create an empty canvas element
                var canvas = document.createElement( "canvas" );
                canvas.width = img.width;
                canvas.height = img.height;
                // Copy the image contents to the canvas
                var ctx = canvas.getContext( "2d" );
                ctx.drawImage( img, 0, 0 );
                // Get the data-URL formatted image
                // Firefox supports PNG and JPEG. You could check img.src to
                // guess the original format, but be aware the using "image/jpg"
                // will re-encode the image.
                var dataURL = canvas.toDataURL("image/jpeg", 1.0);
                return dataURL;
            }

            function initStupidity() {
                scope.stupidity = {
                    category:scope.stupidityCategoryLists[0][0].value,
                    severity:scope.stupidityLevels[0].value,
                    comment:''
                };
            }

        }
    }
}
