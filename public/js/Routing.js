angular
    .module( 'MainApp' )
    .config( function ( $locationProvider, $stateProvider, $urlRouterProvider, $sceDelegateProvider, $httpProvider ) {
        $locationProvider.html5Mode( {
            enabled: true,
            requireBase: false
        } ).hashPrefix( '!' );

        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];

        var host = '//static.'+window.location.host;
        var whitelistHost = window.location.protocol + host + '/**'
        $sceDelegateProvider.resourceUrlWhitelist(['self', whitelistHost]);

        $stateProvider
            .state( 'index', {
                url: "/",
                views: {
                    "viewLeft": {
                        template: ""
                    },
                    "viewRight": {
                        templateUrl: host+"/templates/stupidity-list.html",
                        controller:'StupidityListController',
                        controllerAs: 'StuLiC'
                    }
                }
            } )
            .state( 'stupidity', {
                url: "/stupidity/:stupidityId",

                views: {
                    "viewLeft": {
                        templateUrl: host+"/templates/current-stupidity-image.html",
                        controller: 'CurrentStupidityImageController',
                        controllerAs: 'CurStuImageC'
                    },
                    "viewRight": {
                        templateUrl: host+"/templates/stupidity-list.html",
                        controller:'StupidityListController',
                        controllerAs: 'StuLiC'
                    }
                },

            } )
            .state( 'about', {
                url: "/about",

                views: {
                    "viewLeft": {
                        templateUrl: host+"/templates/about.html",
                        controller:'AboutController',
                        controllerAs: 'AboutC'
                    },
                    "viewRight": {
                        templateUrl: host+"/templates/stupidity-list.html",
                        controller:'StupidityListController',
                        controllerAs: 'StuLiC'
                    }
                },

            } )
            .state( 'complains', {
                url: "/complains",
                views: {
                    "viewLeft": {
                    },
                    "viewRight": {
                    }
                },

            } )

        $urlRouterProvider.otherwise( '/' );
    } );


