angular
    .module( 'MainApp' )
    .factory( 'ExifFactory', function ( $q ) {


        activate();
        var service = {getExifPosition: getExifPosition};
        return service;

        function activate() {
        }

        function getExifPosition( file ) {
            return $q( function ( resolve, reject ) {
                EXIF.getData( file, function () {
                    console.log( EXIF.pretty( this ) );
                    // check if location is in exif
                    var longitude = EXIF.getTag( this, "GPSLongitude" );
                    var latitude = EXIF.getTag( this, "GPSLatitude" );

                    if (longitude && latitude) {
                        latitude = sexadecimalToDecimal( latitude[0], latitude[1], latitude[2] );
                        longitude = sexadecimalToDecimal( longitude[0], longitude[1], longitude[2] );

                        if (EXIF.getTag( this, "GPSLongitudeRef" ) == "W") {
                            longitude = -longitude;
                        }

                        if (EXIF.getTag( this, "GPSLatitudeRef" ) == "S") {
                            latitude = -latitude;
                        }

                        var position = {
                            coords: {
                                longitude: longitude,
                                latitude: latitude
                            }
                        };

                        console.log( "gps from file" );
                        resolve( position )
                    }
                    else
                        reject();
                } );
            } )
        }


        function sexadecimalToDecimal( seconds, minutes, hours ) {
            return seconds + (minutes / 60) + (hours / 3600);
        };
    } );