angular
    .module( 'MainApp' )
    .factory( 'GoogleMapsFactory', function ( $q, $rootScope, $location ) {
        var map;
        var mapIsLoaded = false;
        var mapLoadResolves = [];

        var markers = [];
        var severityMap = {
            0:'https://raw.githubusercontent.com/Concept211/Google-Maps-Markers/master/images/marker_yellow.png',
            1:'https://raw.githubusercontent.com/Concept211/Google-Maps-Markers/master/images/marker_orange.png',
            2:'https://raw.githubusercontent.com/Concept211/Google-Maps-Markers/master/images/marker_red.png'
        };
        var defaultPreviewZoom = 18;

        var previewMarker = {};
        var idiotismHelp = {};

        activate();
        var service = {
            addMarker: addMarker,
            getMap: getMap,
            subscribeOnMapLoad: subscribeOnMapLoad,
            changePreviewMarker: changePreviewMarker,
            setPositionByStupidity: setPositionByStupidity,
            getPreviewMarkerPosition: getPreviewMarkerPosition,
            disablePreviewMarker: disablePreviewMarker
        };
        return service;

        function activate() {
            initMap();
        }

        function getMap() {
            return map;
        }

        function subscribeOnMapLoad() {
            return $q( function ( resolve, reject ) {
                if (!mapIsLoaded) {
                    mapLoadResolves.push( resolve )
                }
                else {
                    resolve();
                }
            } );
        }

        function addMarker( stupidity ) {

            var longitude = parseFloat( stupidity.location[0] );
            var latitude = parseFloat( stupidity.location[1] );

            var newMarker = new google.maps.Marker( {
                position: {lat: latitude, lng: longitude},
                map: map,
                title: '' + stupidity._id,
                icon:severityMap[stupidity.severity]
                // label: ''+ stupidity.severity
            } );

            newMarker.id = stupidity._id;

            markers.push( newMarker );

            google.maps.event.addListener( newMarker, "click", function ( e ) {
                //location.href = "#idiotism-" + id;
                map.setCenter( newMarker.position );
                $location.path( '/stupidity/' + newMarker.id );
                $rootScope.$apply();
            } );
        }

        function setPositionByStupidity( stupidity ) {
            if (map) {
                var center = {
                    lat: parseFloat( stupidity.location[1] ),
                    lng: parseFloat( stupidity.location[0] )
                };
                map.setZoom( defaultPreviewZoom );
                map.setCenter( center );
            }
        }

        function changePreviewMarker( position ) {
            if (position) {
                onPositionFound( position )
            }
            else {
                previewMarker.setPosition( map.getCenter() );
                previewMarker.setMap( map );
                idiotismHelp.open( map, previewMarker );
                navigator.geolocation.getCurrentPosition( onPositionFound, onPositionError );
            }
        }

        function disablePreviewMarker() {
            previewMarker.setMap( null );
        }

        function getPreviewMarkerPosition() {
            return previewMarker.getPosition();
        }

        function initMap() {
            if (typeof google != "undefined") {
                var center = {lat: 46.438173, lng: 30.73068046};

                var mapOptions = {
                    center: center,
                    zoom: 12
                };

                map = new google.maps.Map( document.getElementById( 'map-canvas' ), mapOptions );

                var pinColor = "00FF00";
                var pinImage = new google.maps.MarkerImage(
                    "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
                    new google.maps.Size( 21, 34 ),
                    new google.maps.Point( 0, 0 ),
                    new google.maps.Point( 10, 34 )
                );

                previewMarker = new google.maps.Marker( {
                    position: center,
                    map: null,
                    draggable: true,
                    icon: pinImage,
                    title: "Show me some stupidity!"
                } );

                google.maps.event.addListenerOnce( map, 'tilesloaded', function () {
                    mapLoadResolves.forEach( function ( resolve ) {
                        resolve();
                    } );
                    mapIsLoaded = true;
                } );

                idiotismHelp = new google.maps.InfoWindow( {
                    content: 'Перетащи меня туда, где было снято фото дебилизма!'
                } );
            }
        }

        function onPositionFound( position ) {
            console.log( "got gps!" );
            var center = {lat: position.coords.latitude, lng: position.coords.longitude};

            previewMarker.setMap( map );
            previewMarker.setPosition( center );
            idiotismHelp.open( map, previewMarker );
            map.setZoom( defaultPreviewZoom );
            map.setCenter( center );
        };

        function onPositionError( error ) {
            switch (error.code) {
                case error.PERMISSION_DENIED:
                    console.log( "User denied the request for Geolocation." );
                    break;
                case error.POSITION_UNAVAILABLE:
                    console.log( "Location information is unavailable." );
                    break;
                case error.TIMEOUT:
                    console.log( "The request to get user location timed out." );
                    break;
                case error.UNKNOWN_ERROR:
                    console.log( "An unknown error occurred." );
                    break;
            }
        };

    } );