angular
    .module( 'MainApp' )
    .factory( 'ComplainFactory', function ( $q, $http, feathers ) {
        var init = false;

        const Complains = feathers.getApp().service( 'stupidities/:stupidityId/report' );

        activate();
        var service = {
            create: create,
            find:find
        };
        return service;

        function activate() {
        }


        function create( complain ) {
            if (!complain.stupidityId || !complain.comment){
                console.error('You should send comment and id')
            }
            return Complains.create( complain ).then( function ( response ) {
                console.log( response );
            } ).catch( function ( err ) {
                console.log( err );
            } )
        }
        
        function find() {
            return Complains.find().then( function ( response ) {
                    console.log( response );
                } ).catch( function ( err ) {
                    console.log( err );
                } )
        }
    } );