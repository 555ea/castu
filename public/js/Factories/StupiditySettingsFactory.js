angular
    .module( 'MainApp' )
    .factory( 'StupiditySettingsFactory', function ($q,$http) {

        activate();
        return {
            getStupidityLevels: getStupidityLevels,
            getStupidityCategoryLists:getStupidityCategoryLists,
        };

        function activate() {
        }

        function getStupidityLevels() {
            return [
                {
                    value: '0',
                    text: '\nИсправимо\nгражданами',
                    icon: 'glyphicon-user'
                },
                {
                    value: '1',
                    text: '\nИсправимо\nгосударством',
                    icon: 'glyphicon-eye-open'
                },
                {
                    value: '2',
                    text: '\nНеисправимо.\nГосподь, жги.',
                    icon: 'glyphicon-fire'
                }
            ];
        }

        function getStupidityCategoryLists() {
            return [
                [
                    {
                        value: '0',
                        text: '\nНаглое\nжлобство',
                        icon: 'glyphicon-piggy-bank',
                    },
                    {
                        value: '1',
                        text: '\nВизуальный\nмусор',
                        icon: 'glyphicon-eye-close'
                    },
                    {
                        value: '2',
                        text: '\nАпокалиптические\nдороги',
                        icon: 'glyphicon-road'
                    }
                ],
                [
                    {
                        value: '3',
                        text: '\nЭкологические\nкатастрофы',
                        icon: 'glyphicon-globe'
                    },
                    {
                        value: '4',
                        text: '\nЭнергетическое\nтранжирство',
                        icon: 'glyphicon-scale'
                    },
                    {
                        value: '5',
                        text: '\nИздевательство\nнад инвалидами',
                        icon: 'glyphicon-sunglasses'
                    }
                ],
                [
                    {
                        value: '6',
                        text: '\nРазруха\nна пороге',
                        icon: 'glyphicon-home'
                    },
                    {
                        value: '7',
                        text: '\nМуниципальная\nдегенерация',
                        icon: 'glyphicon-barcode'
                    },
                    {
                        value: '8',
                        text: '\nСепаратизм\nи нетерпимость',
                        icon: 'glyphicon-rub'
                    }
                ],
            ]
        }

    } );