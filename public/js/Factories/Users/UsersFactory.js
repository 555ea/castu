angular
    .module('MainApp')
    .factory('UsersFactory', function ($q, $http, $window, $interval, hosts, feathers) {

        var app = feathers.getApp();
        var user = undefined;
        const User = app.service('user');

        activate();
        var service = {
            authorizeVK: authorizeVK,
            authorizeFacebook: authorizeFacebook,
            find: find,
            getUser: getUser,
            initialAuthenticate: initialAuthenticate
        };
        return service;

        function activate() {
        }

        function authorizeVK() {
            return showSeparateWindow('/auth/vkontakte');
        }

        function authorizeFacebook() {
            return showSeparateWindow('/auth/facebook');
        }

        function showSeparateWindow(url) {
            return new $q(function (resolve, reject) {
                // center the popup window
                var left = screen.width / 2 - 200
                    , top = screen.height / 2 - 250
                    , popup = $window.open(url, '', "top=" + top + ",left=" + left + ",width=400,height=500")
                    , interval = 1000;

                // create an ever increasing interval to check a certain global value getting assigned in the popup
                var i = $interval(function () {
                    interval += 500;
                    try {
                        if (popup.closed) {
                            var app = feathers.getApp();

                            var tokenLocation = 'feathers-jwt';
                            var token = getCookie(tokenLocation);

                            app.authenticate({
                                type: 'token',
                                'token': token
                            }).then(function (result) {
                                console.log('Authenticated!', app.get('token'));
                                User.find().then(function (userFromDB) {
                                    user = userFromDB;
                                    resolve();
                                })
                            }).catch(function (error) {
                                console.error('Error authenticating!', error);
                            });

                            $interval.cancel(i);
                        }
                    } catch (e) {
                        console.error(e);
                        reject();
                    }
                }, interval);
            })
        }

        function find() {
            User.find()
                .then(function (response) {
                    console.log(response);
                    return response;
                })
                .catch(function (error) {
                    console.log(error)
                });
        }


        function upgradeUser(user) {
            if (user.fb) {

            } else if (user.vk) {
                $http.get('https://api.vk.com/method/users.get?user_ids=' + user.vk.id + '&fields=photo_50')
                    .then(function (response) {
                        console.log(response);
                    }).catch(function (error) {
                    console.log(error);
                })
            }
        }

        function getCookie(name) {
            if (typeof document !== 'undefined') {
                var value = '; ' + document.cookie;
                var parts = value.split('; ' + name + '=');
                if (parts.length === 2) {
                    return parts.pop().split(';').shift();
                }
            }
            return null;
        }

        function initialAuthenticate() {
            return new $q(function (resolve, reject) {
                app.authenticate().then(function (result) {
                    return User.find().then(function (userFromDB) {
                        user = userFromDB;
                        resolve(user);
                    })
                }).catch(function (error) {
                    user = null;
                    console.log('Error authenticating!', error);
                    reject();
                });
            })
        }

        function getUser() {
            return user;
        }

    });