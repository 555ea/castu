angular
    .module( 'MainApp' )
    .factory( 'VKFactory', function ( $http ) {

        var likes = [];
        var commentBars = [];

        activate();
        var service = {createComments: createComments, createLikes: createLikes};
        return service;

        var VK = window.VK;

        function activate() {
            if ( VK != undefined ) {
                VK.init( {apiId: 5120321, onlyWidgets: true} );

                VK.Auth.getLoginStatus( function ( response ) {
                    if ( response.session ) {
                        console.log( 'got' );
                    } else {
                        console.log( 'try login' );
                        VK.Auth.login( function ( response ) {
                            if ( response.session ) {
                                console.log( 'loggedin' );
                            } else {
                                console.log( 'not logged in' );
                            }
                        } );
                    }
                } );
            }
        }


        function createComments( id ) {
            if ( VK != undefined ) {
                commentBars.push( id );
                VK.Widgets.Comments( id, {limit: 1, width: "400", attach: "*"}, id );
            }
        };

        function createLikes( id ) {
            if ( VK != undefined ) {
                //likes.push( id );
                //VK.Widgets.Like( id, {type: "button", verb: 1, height: 22}, id );
            }
        };


    } );
