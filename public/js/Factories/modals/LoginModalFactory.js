angular
    .module( 'MainApp' )
    .factory( 'LoginModalFactory', function ($q, $uibModal, hosts) {
        var host;

        activate();
        var service = {open: open};
        return service;

        function activate() {
            host = hosts.getStatic();
        }

        function open(){
            return new $q( function ( resolve, reject ) {
                $uibModal.open( {
                    templateUrl: host+'/templates/modals/login.html',
                    controller: 'LoginModalController',
                    controllerAs:'LoginC',
                    size:'sm' //TODO
                } ).result.then( function ( result ) {
                        resolve( result );
                    } );
            } );
        }

    } );