angular
    .module('MainApp')
    .factory('ComplainModalFactory', function ($q, $uibModal, hosts) {
        var host;

        activate();
        var service = {complainOn: complainOn};
        return service;

        function activate() {
            host = hosts.getStatic();
        }

        function complainOn(stupidity) {
            return new $q(function (resolve, reject) {
                $uibModal.open({
                    templateUrl: host + '/templates/modals/complain.html',
                    controller: 'ComplainModalController',
                    controllerAs: 'vm',
                    resolve: {
                        stupidity: function () {
                            return stupidity;
                        }
                    },
                    size: 'sm' //TODO
                }).result.then(function (result) {
                    resolve(result);
                });
            });
        }

    });