angular
    .module('MainApp')
    .factory('StupidityFactory', function ($q, $http, feathers, hosts, GoogleMapsFactory) {
        var init = false;

        const Stupidities = feathers.getApp().service('stupidities');

        var stupiditiesArray = [];

        //     {
        //     _id: 0,
        //     image: "https://pp.vk.me/c630717/v630717221/2827/8ossLpXa_Ag.jpg",
        //     comment: "Ukraine: In Mariupol, 172 patrol police officers have taken an oath today and will start patrolling immediately.  ",
        //     location: ["30.729349", "46.431499"],
        //     voteCount:1,
        //     severity:0,
        //     category:0
        // }, {
        //     _id: 1,
        //     image: "https://pp.vk.me/c7007/v7007628/40178/bY55m4ns-Tc.jpg",
        //     comment: "Ukraine: In Mariupol, 172 patrol police officers have taken an oath today and will start patrolling immediately.  ",
        //     location: ["30.720349", "46.439499"],
        //     voteCount:1,
        //     severity:1,
        //     category:2
        // },
        //     {
        //         _id: 2,
        //         image: "https://pp.vk.me/c7007/v7007628/40178/bY55m4ns-Tc.jpg",
        //         comment: "Ukraine: In Mariupol, 172 patrol police officers have taken an oath today and will start patrolling immediately.  ",
        //         location: ["30.731949", "46.439499"],
        //         voteCount:0,
        //         severity:2,
        //         category:3
        //     }];

        activate();
        var service = {
            find: find,
            getStupidityById: getStupidityById,
            setStupidities: setStupidities,
            isInit: isInit,
            deleteStupidity: deleteStupidity,
            create: create
        };
        return service;

        function activate() {
            stupiditiesArray.forEach(function (stupidity) {
                GoogleMapsFactory.addMarker(stupidity);
            })
            subscribeOnCreate();
        }

        function isInit() {
            return init;
        }

        function setStupidities(newStupidities) {
            stupiditiesArray = newStupidities;
            init = true;
        }

        function initImageInStupidity(stupidity) {
            if (stupidity && stupidity.image.indexOf('images') == -1) {
                stupidity.image = hosts.getStatic() + '/images/' + stupidity.image;
            }
            return stupidity;
        }

        function find() {
            return new $q(function (resolve, reject) {
                if (stupiditiesArray.length <= 0) {
                    Stupidities.find()
                        .then(function (stupidities) {
                            console.log(stupidities);
                            stupidities.forEach(function (stupidity) {
                                initImageInStupidity(stupidity);
                            });
                            stupiditiesArray = stupidities;
                            resolve(stupiditiesArray);
                        })
                        .catch(function (error) {
                            console.log(error)
                            reject(error);
                        });
                } else {
                    resolve(stupiditiesArray);
                }
            })
        }

        function create(stupidity) {
            return Stupidities.create(stupidity).then(function (response) {
                console.log(response);
            }).catch(function (err) {
                console.log(err);
            })
        }

        function subscribeOnCreate() {
            Stupidities.on('created', function (stupidity) {
                stupidity.image = hosts.getStatic() + '/images/' + stupidity.image;
                stupiditiesArray.unshift(stupidity);
            });
        }

        function getStupidityById(id) {
            return new $q(function (resolve, reject) {
                var stupidity = _.findWhere(stupiditiesArray, {_id: id})
                if (stupidity) {
                    resolve(stupidity);
                } else {
                    find(); // TODO: not sure yet
                    resolve(get(id));
                }
            });
        }

        function get(stupidityId) {
            return Stupidities.get(stupidityId).then(function (response) {
                console.log(response);
                var stupidity = initImageInStupidity(response);
                return stupidity;
            }).catch(function (err) {
                console.log(err);
            })
        }

        function deleteStupidity(stupidity) {
            return $q(function (resolve, reject) {
                $http({
                    method: 'DELETE',
                    url: window.location.protocol + '//api.' + window.location.hostname + '/stupidity/' + stupidity._id,
                }).then(function (data) {
                    console.log('stupidity deleted!');
                    stupiditiesArray = _.without(stupiditiesArray, stupidity);
                    resolve(stupiditiesArray);
                }).catch(function (err) {
                    console.log('Error deleting stupidity');
                    console.log(err);
                    reject();
                })
            });
        }
    });