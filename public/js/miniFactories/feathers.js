angular
    .module('MainApp')
    .factory('feathers', function (hosts) {

        var host;
        var socket;
        var app;

        activate();
        var service = {getApp:getApp};
        return service;

        function activate() {
            host = hosts.getApi();
            socket = io(host);
            app = feathers()
                .configure(feathers.socketio(socket))
                .configure(feathers.hooks())
                .configure(feathers.authentication({ storage: window.localStorage }));
        }

        function getApp() {
            return app;
        }

    });