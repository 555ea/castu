angular
    .module( 'MainApp' )
    .factory( 'hosts', function () {

        var host;
        var protocol;

        activate();
        var service = {getStatic: getStatic, getApi: getApi, getAuth:getAuth};
        return service;

        function activate() {
            host = window.location.host;
            protocol = window.location.protocol;
        }

        function getStatic(){
            return protocol+'//static.' + host;
        }
        function getApi(){
            return protocol+'//api.' + host;
        }
        function getAuth(){
            return protocol+'//auth.' + host;
        }
    } );