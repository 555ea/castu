angular
    .module('MainApp')
    .controller('BodyController', function ($scope, $location, $state, LoginModalFactory, UsersFactory, ComplainFactory) {

        var vm = this; // ViewModel


        // functions
        vm.getUser = getUser;
        vm.login = login;
        vm.complains = complains;
        vm.formatUser = formatUser;
        vm.getActiveClassIfLocation = getActiveClassIfLocation;


        function activate() {
            UsersFactory.initialAuthenticate().then(getUser);
        }

        function login() {
            LoginModalFactory.open();
        }

        function complains() {
            // ComplainFactory.find();
        }

        function getUser() {
            return UsersFactory.getUser();
        }

        function formatUser() {
            var user = getUser();
            if(user.fb){
                user.displayName = user.fb.name + ', через facebook.com';
                user.avatar = user.fb.avatar;
                user.usedStrategy = 'fb';
            }else{
                if (user.vk){
                    user.displayName = user.vk.name + ', через vk.com';
                    user.avatar = user.vk.avatar;
                    user.usedStrategy = 'vk';
                }
            }
            return user;
        }

        function getActiveClassIfLocation(path) {
            return ($location.path().substr(0, path.length) === path) ? 'active' : '';
        }

        activate();
    })