angular
    .module('MainApp')
    .controller('CurrentStupidityImageController', function ($stateParams, $location, StupidityFactory, GoogleMapsFactory, $http) {
            var vm = this; // ViewModel

            vm.stupidity = {}

            function activate() {
                StupidityFactory.getStupidityById($stateParams.stupidityId).then(function (stupidity) {
                    if (stupidity) {
                        setStupidity(stupidity);
                    }
                });
            }

            function setStupidity(stupidity) {
                vm.stupidity = stupidity;
                GoogleMapsFactory.setPositionByStupidity(vm.stupidity);
            }

            vm.hide = function () {
                $location.path('/');
            }

            activate();
        }
    )