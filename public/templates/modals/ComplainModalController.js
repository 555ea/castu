angular
    .module('MainApp')
    .controller('ComplainModalController', function ($uibModalInstance, stupidity, ComplainFactory) {
            var vm = this; // ViewModel

            vm.complainTypes = [
                {name:'Неправильно указана локация',
                    value:'Тупизма здесь несуществует, либо он находится недалеко, но не прямо тут'},
                {name:'Это не фото тупизма',
                    value:'К сожалению прикрепленное фото не является реальным фото тупизма'},
                {name:'Другое',
                    value:'Тупизм нарушает правила, но я не смог указать точную причину'},
            ];

            vm.onTypeSelected = function(value){
                vm.comment = value || '';
            }

            vm.ok = function () {
                ComplainFactory.create({stupidityId: stupidity._id, comment: vm.comment}).then(function () {
                    $uibModalInstance.close(true);
                })
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('Cancel');
            };

            function activate() {
                vm.lazy = true;
                vm.stupidity = stupidity;
                vm.selectedItem = vm.complainTypes[0].value;
                vm.comment = vm.selectedItem;
            }

            activate();
        }
    )