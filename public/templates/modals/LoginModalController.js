angular
    .module( 'MainApp' )
    .controller( 'LoginModalController', function ( $uibModalInstance, UsersFactory ) {
        var vm = this; // ViewModel

        vm.ok = function () {
            $uibModalInstance.close( true );
        };

        vm.cancel = function () {
            $uibModalInstance.dismiss( 'Cancel' );
        };

        vm.vk = function () {
            UsersFactory.authorizeVK().then(function(){
                $uibModalInstance.close( true );
            })
        };

        vm.facebook = function () {
            UsersFactory.authorizeFacebook().then(function(){
                $uibModalInstance.close( true );
            })
        };

        function activate() {
        }

        activate();
    }
)