angular
    .module( 'MainApp' )
    .controller( 'StupidityListController', function ( $scope, $http, $state,
                                                       StupidityFactory, StupiditySettingsFactory, ComplainModalFactory, GoogleMapsFactory,
                                                       hosts ) {
        var vm = this; // ViewModel

        vm.stupidities = [];
        vm.sliderOptions = {
            minRange:0,
            maxRange:10,
            showSelectionBar: true
        };

        vm.getStupidityClasses = getStupidityClasses;
        vm.deleteStupidity = deleteStupidity;
        vm.getGlyphiconClass = getGlyphiconClass;
        vm.complainOn = complainOn;

        activate();

        function activate() {
            vm.stupidityTemplate = hosts.getStatic() + '/templates/stupidity.html';
            StupidityFactory.find().then(function(stupidities){
                vm.stupidities = stupidities;
                vm.stupidities.forEach(GoogleMapsFactory.addMarker);
            });
            vm.categoryLists = StupiditySettingsFactory.getStupidityCategoryLists();
        }

        function getStupidityClasses(stupidity) {
            var map = {0:'yellow',1:'orange',2:'red'};
            var classes = [];
            classes.push(map[stupidity.severity])
            return classes;
        }

        function getGlyphiconClass(stupidity) {
            var glyphicon = '';
            vm.categoryLists.forEach(function (categoryList) {
                var category = _.findWhere(categoryList, {value:''+stupidity.category});
                if (category){
                    glyphicon = category.icon;
                }
            })
            return glyphicon;
        }

        function deleteStupidity( stupidity ) {
            StupidityFactory.deleteStupidity( stupidity ).then( function ( stupidities ) {
                vm.stupidities = stupidities;
            } );
        }

        function complainOn(stupidity) {
            ComplainModalFactory.complainOn(stupidity);
        }

    } )