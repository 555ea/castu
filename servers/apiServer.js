var feathers = require('feathers');
var rest = require('feathers-rest');
var socketio = require('feathers-socketio');
var hooks = require('feathers-hooks');
var authentication = require('feathers-authentication');

var authHooks = authentication.hooks;
var bodyParser = require('body-parser');

var VKStrategy = require('passport-vkontakte').Strategy;
var FBStrategy = require('passport-facebook').Strategy;

var errors = require('../api/helpers/errors');

var Stupidity = require('../api/models/stupidity');
var StupidityVote = require('../api/models/stupidityVote');
var Fix = require('../api/models/stupidityFix');
var FixVote = require('../api/models/stupidityFixVote');
var Report = require('../api/models/stupidityReport');
var ReportPart = require('../api/models/stupidityReportPart');
var User = require('../api/models/user');

var stupiditiesService = require('../api/services/stupidities')(Stupidity);
var stupidityFixingService = require('../api/services/stupidityFixing')(Fix, FixVote, Stupidity, stupiditiesService);
var stupidityFixingVotingService = require('../api/services/stupidityFixingVoting')(Fix, FixVote, stupidityFixingService);
var stupidityReportingService = require('../api/services/stupidityReporting')(Stupidity, Report, ReportPart);
var stupidityReportingVotingService = require('../api/services/stupidityReportingVoting')(Stupidity, Report, ReportPart);
var stupidityVotingService = require('../api/services/stupidityVoting')(Stupidity, StupidityVote, stupiditiesService);

var userService = require('../api/services/user')(Stupidity, Fix, Report, ReportPart, User);

var _ = require('lodash');

module.exports = function(config, authConfig) {
    var app = feathers();

    app.configure(rest());
    app.configure(socketio());
    app.configure(hooks());

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());

    var endpoints = {
	stupidities: '/stupidities',
	fixes: '/stupidities/:stupidityId/fix',
	fixesVotes: '/stupidities/:stupidityId/fix/:fixId/vote',
	reports: '/stupidities/:stupidityId/report',
	reportsVotes: '/stupidities/:stupidityId/report/vote',
	votes: '/stupidities/:stupidityId/vote',
	users: '/user',
	echo: '/echo/:echoId/echo'
    };

    if(authConfig) {
	app.configure(authentication({
	    local: false, // we use our own authentication provider
	    successRedirect: false,
	    failureRedirect: false,
	    token: authConfig.jwt
	}));
    }

    app.use(endpoints.stupidities, stupiditiesService);
    app.use(endpoints.fixes, stupidityFixingService);
    app.use(endpoints.fixesVotes, stupidityFixingVotingService);
    app.use(endpoints.reports, stupidityReportingService);
    app.use(endpoints.reportsVotes, stupidityReportingVotingService);
    app.use(endpoints.reports, stupidityReportingService);
    app.use(endpoints.votes, stupidityVotingService);
    app.use(endpoints.users, userService);
    app.use(endpoints.echo, {
	get: function(params) {
	    debugger;
	    return Promise.resolve({params: params});
	},
	create: function(data, params) {
	    debugger;
	    return Promise.resolve({data: data, params: params.query});
	}
    });

    if(authConfig) {
	var totallyProtectedEndpoints = [
	    endpoints.users,
	    endpoints.fixes,
	    endpoints.fixesVotes,
	    endpoints.reports,
	    endpoints.reportsVotes
	];

	function populateUserHook(hook) {
	    return new Promise((resolve, reject) => {
		User.findById(hook.params.payload.id).then(user => {
		    hook.params.user = user;
		    resolve(hook);
		}).catch(error => {
		    reject(hook);
		});
	    });
	}

	function copyDataToParams(hook) {
	    return new Promise((resolve, reject) => {
		for(var key in hook.data) {
		    if(key in hook.params) {
			continue;
		    } else {
			hook.params[key] = hook.data[key];
		    }
		}
		resolve(hook);
	    });
	}

	_.forEach(totallyProtectedEndpoints, endpoint => {
	    app.service(endpoint).before({
		all: [
		    authHooks.verifyToken(),
		    populateUserHook,
		    copyDataToParams
		]
	    });
	});

	app.service(endpoints.stupidities).before({
	    create: [
		authHooks.verifyToken(),
		populateUserHook,
		copyDataToParams
	    ]
	});

	app.service('/auth/token').after({
	    all: (hook) => {
		// use our own token with our own payload
		if(hook.result.token && hook.params.token) {
		    hook.result.token = hook.params.token;
		}
	    }
	});
    } else {
	console.warn("Api server runs with no authentication.");
    }

    app.use(function(err, req, res, next) {
        res.status(err.code || 500);

	// TODO: log error if it's something of interest
	if(err.code in errors) {
	    err = errors[err.code];
	} else {
	    // make sure we don't leak anything
	    err = _.pick(err, ['code', 'message']);
	}

        res.format({
            'application/json': function() {
                res.json(err);
            },

            'text/plain': function() {
                res.send(err.message);
            }
        });
    });

    return app;
};
