var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var request = require('request');

var errors = require('../api/helpers/errors');

module.exports = (config, authConfig, tokenService, User) => {
    var app = express();
    app.use(cookieParser());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());

    var helpers = require('../api/helpers/auth')(authConfig, tokenService, User);

    var hostname = 
	'http' + (config.useSSL ? 's' : '') + '://' +
	config.subdomains.auth + config.hostname + ':' + config.apiPort + '/auth';

    function requireFieldInQuery(field) {
	return (req, res, next) => {
	    if(field in req.query) {
		next();
	    } else {
		res.redirect('/auth/access/denied');
	    }
	};
    }

    app.get('/auth/access/granted', helpers.extractJWT, (req, res) => {
	if(!req.params.jwt) {
	    res.redirect('/auth/access/denied');
	} else {
	    helpers.findUserByJWT(req.params.jwt).then(() => {
		res.sendFile(path.join(config.appRoot, '/public/authSuccess.html'));
	    }).catch(() => {
		res.redirect('/auth/access/denied');
	    });
	}
    });

    app.get('/auth/access/denied', (req, res) => {
	res.json(errors[403]);
    });
    
    app.get('/auth/vkontakte', (req, res) => {
	var authURL = 'https://oauth.vk.com/authorize' +
	    '?client_id=' + authConfig.vk.clientID +
	    '&redirect_uri=' + hostname + '/vkontakte/callback' +
	    '&display=page';

	res.redirect(authURL);
    });
    
    app.get('/auth/vkontakte/callback', requireFieldInQuery('code'), helpers.extractJWT, (req, res) => {
	var redirectUri = hostname + '/vkontakte/callback';

	var tokenURL = 'https://oauth.vk.com/access_token' +
	    '?client_id=' + authConfig.vk.clientID +
	    '&client_secret=' + authConfig.vk.clientSecret + 
	    '&redirect_uri=' + redirectUri +
	    '&code=' + req.query.code;

	request.get(tokenURL, (error, response, body) => {
	    var body = JSON.parse(body);
	    
	    if(error || !body.error) {
		var userID = body.user_id;
		var token = body.access_token;

		if(!userID || !token) {
		    // TODO: vk problem? log error
		    res.redirect('/auth/access/denied');
		}

		helpers.authenticationProcess(userID, token, req.params.jwt, 'vk')
		    .then(jwt => {
			helpers.saveCookie(res, jwt);
			res.redirect('/auth/access/granted');
		    }).catch(error => {
			// TODO: that's strange, log error
			res.redirect('/auth/access/denied');
		    });
	    } else {
		// TODO: log error, may be a hacker
		res.redirect('/auth/access/denied');
	    }
	});
    });

    app.get('/auth/facebook', (req, res) => {
	var authURL = 'https://www.facebook.com/dialog/oauth' +
	    '?client_id=' + authConfig.fb.clientID +
	    '&redirect_uri=' + hostname + '/facebook/callback';

	res.redirect(authURL);
    });
    
    app.get('/auth/facebook/callback', requireFieldInQuery('code'), helpers.extractJWT, (req, res) => {
	var tokenURL = 'https://graph.facebook.com/v2.3/oauth/access_token' +
	    '?client_id=' + authConfig.fb.clientID +
	    '&client_secret=' + authConfig.fb.clientSecret + 
	    '&redirect_uri=' + hostname + '/facebook/callback' +
	    '&code=' + req.query.code;

	request.get(tokenURL, (error, response, body) => {
	    var body = JSON.parse(body);
	    
	    if(error || !body.error) {
		var token = body.access_token;

		// fb won't make it easy for us to get user ID
		var tokenDebugURL = 'https://graph.facebook.com/debug_token' + 
		    '?input_token=' + token +
		    '&access_token=' + authConfig.fb.clientID + '|' + authConfig.fb.clientSecret;
		
		request.get(tokenDebugURL, (error, response, body) => {
		    var body = JSON.parse(body);

		    if(!body.error) {
			var userID = body.data.user_id;

			if(!userID) {
			    // TODO: fb problem? log error
			    res.redirect('/auth/access/denied');
			}

			helpers.authenticationProcess(userID, token, req.params.jwt, 'fb')
			    .then(jwt => {
				helpers.saveCookie(res, jwt);
				res.redirect('/auth/access/granted');
			    }).catch(error => {
				// TODO: that's strange, log error
				res.redirect('/auth/access/denied');
			    });
		    } else {
			// TODO: that's strange, something with FB
			res.redirect('/auth/access/denied');
		    }
		});
	    } else {
		// TODO: log error, may be a hacker
		res.redirect('/auth/access/denied');
	    }
	});
    });

    return app;
};
