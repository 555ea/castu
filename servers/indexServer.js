var favicon = require('serve-favicon');
var path = require('path');
var express = require('express');

module.exports = function(app, config) {
    app.use(favicon(config.appRoot + '/public/favicon.png'));

    // unneeded in case of cloudflare
    //app.use(express.static(__dirname + '/../public', { maxAge: '12h' }));

    app.get('/*', function(req, res){
        res.sendFile( path.join( config.appRoot, '/views/index.html' ) );
    });

    return app;
}
