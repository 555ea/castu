var path = require('path');
var express = require('express');

module.exports = function(config) {
    var app = express();

    app.use('/images', express.static(
        config.appRoot + '/public/images',
        {
            setHeaders: function(res, path, stat) {
                // ensure that we serve images as images, and nothing else
                res.contentType('image/xyz');
            }
        })
    );

    app.use(express.static(__dirname + '/../public', { maxAge: 0 }));

    return app;
};
