'use strict';

var fs = require('fs');
var http = require('http');
var https = require('https');
var vhost = require('vhost');
var express = require('express');
var mongoose = require('mongoose');

var jwt = require('jsonwebtoken');
var User = require('./api/models/user.js');

mongoose.connect('localhost', 'stupidity');
mongoose.Promise = global.Promise;

var config = {
    appRoot: __dirname,
    useSSL: false,
    apiPort: 3000,
    hostname: 'localhost',
    subdomains: {
	index: '',
	api: 'api.',
	static: 'static.',
	auth: ''
    }
};

var authConfig = {
    jwt: {
	secret: 'STUPIDITY LEADS TO ERECTILE DISABILITY',
	issuer: 'castu',
	expiresIn: '7d',
	algorithm: 'HS256'
    },
    vk: {
	clientID: '4964996',
	clientSecret: 'OJ5Y1sG3mIvM9VtT64jm',
    },
    fb: {
	clientID: '1744060262506223',
	clientSecret: '0905f12f77f398b68ae985df14a54748',
    }
}

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*'); //TODO
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}


var apiApp = require('./servers/apiServer')(config, authConfig);
var staticApp = require('./servers/staticServer')(config);
var authServer = require('./servers/authServer')(config, authConfig, jwt, User);
var indexApp = require('./servers/indexServer')(authServer, config);

var mainApp = express();

mainApp.use(allowCrossDomain);
mainApp.use(vhost(config.hostname, indexApp));
mainApp.use(vhost('api.' + config.hostname, apiApp));
mainApp.use(vhost('static.' + config.hostname, staticApp));

var server = function() {
    if(config.useSSL) {
        var httpsConfig = {
            key: fs.readFileSync('/root/ssl/key.pem'),
            cert: fs.readFileSync('/root/ssl/cert.pem')
        };

        return https.createServer(httpsConfig, mainApp);
    } else {
        return http.createServer(mainApp);
    }
}();

server.listen(config.apiPort);
apiApp.setup(server);
