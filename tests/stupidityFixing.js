var assert = require('chai').assert;
var mongoose = require('mongoose');
var _ = require('lodash');

var apiServer = require('../servers/apiServer')();
var stupidityService = apiServer.service('/stupidities');
var fixingService = apiServer.service('/stupidities/:stupidityId/fix');
var fixingVotingService = apiServer.service('/stupidities/:stupidityId/fix/:fixId/vote');

var Stupidity = require('../api/models/stupidity');
var Fix = require('../api/models/stupidityFix');
var FixVote = require('../api/models/stupidityFixVote');
var User = require('../api/models/user');

var validators = require('../api/helpers/validators');

var stupidityCount = 1;

var testImage = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAAEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKAP/2Q==';

describe('Stupidities', () => {
    var user = {};
    var users = {};

    beforeEach(done => {
	users = _.map(_.range(10), () => {return new User();});

	Promise.all(_.map(users, user => {
	    return user.save();
	})).then(users => {
	    user = users[0];

	    Promise.all(
		_.map(
		    _.range(stupidityCount),
		    (i) => {
			var stupidity = new Stupidity({
			    location: [i, i],
			    image: "image_name",
			    severity: 0,
			    category: 0,
			    submitterId: user._id
			});

			// optional fields 
			if(i % 2) {
			    stupidity.comment = "lorem ipsum";
			}

			return stupidity.save();
		    }
		)
	    ).then(() => {
		User.count().then(count => {
		    done();
		});
	    });
	}).catch(done);
    });

    afterEach(done => {
	mongoose.connection.db.dropDatabase().then(() => {
	    done();
	});
    });

    describe('fixing', () => {
	describe('errors', () => {
	    describe('voting', () => {
		describe('should 400 on invalid data',  () => {
		    it('invalid vote count', done => {
			Stupidity.findOne().then(stupidity => {
			    var ids = {
				user: users[1],
				stupidityId: stupidity.id
			    };
			    var newFix = {
				image: testImage,
				comment: "New Comment"
			    };

			    fixingService.create(_.clone(newFix), ids).then(fix => {
				setTimeout(() => {
				    assert.notEqual(fix.submitterId, user.id);
				});
				var invalidVotes = [-5, 11];

				var promises = _.map(invalidVotes, vote => {
				    return fixingVotingService.get(vote, {
					user: user,
					comment: "comment",
					stupidityId: stupidity.id,
					fixId: fix.id
				    });
				});

				return new Promise((resolve, reject) => {
				    var catched = [];

				    var gotAnswer = _.after(promises.length, () => {
					if(catched.length < promises.length) {
					    setTimeout(() => {
						assert.fail('All promises should have failed');
						gotAnswer();
					    });
					} else {
					    resolve(catched);
					}
				    });

				    _.forEach(promises, promise => {
					promise.then(gotAnswer).catch(error => {
					    catched.push(error);
					    gotAnswer();
					});
				    });
				});
			    }).then(errors => {
				setTimeout(() => {
				    _.forEach(errors, error => {
					assert.equal(error.code, 400);
				    });

				    done();
				});
			    });
			});
		    });
		});

		it('should 403 for unauthorized or banned user', done => {
		    Stupidity.findOne().then(stupidity => {
			user.isBanned = true;

			var ids = {
			    user: users[1],
			    stupidityId: stupidity.id
			};
			var newFix = {
			    image: testImage,
			    comment: "New Comment"
			};

			fixingService.create(_.clone(newFix), ids).then(fix => {
			    setTimeout(() => {
				assert.notEqual(fix.submitterId, user.id);
			    });
			    return fixingVotingService.get(5, {
				user: user,
				comment: "comment",
				stupidityId: stupidity.id,
				fixId: fix.id
			    });
			}).then(() => {
			    setTimeout(() => {
				assert.fail('Should have failed');
				user.isBanned = false;
				done();
			    });
			}).catch(error => {
			    setTimeout(() => {
				assert.equal(error.code, 403);
				user.isBanned = false;
				done();
			    });
			});
		    });
		});

		it('should 401 on trying to vote twice', done => {
		    Stupidity.findOne().then(stupidity => {
			var ids = {
			    user: users[1],
			    stupidityId: stupidity.id
			};
			var newFix = {
			    image: testImage,
			    comment: "New Comment"
			};

			var createdFix = {};
			fixingService.create(_.clone(newFix), ids).then(fix => {
			    setTimeout(() => {
				assert.notEqual(fix.submitterId, user.id);
			    });
			    createdFix = fix;
			    return fixingVotingService.get(5, {
				user: user,
				comment: "comment",
				stupidityId: stupidity.id,
				fixId: createdFix.id
			    });
			}).then(() => {
			    setTimeout(() => {
				assert.notEqual(createdFix.submitterId, user.id);
			    });
			    return fixingVotingService.get(5, {
				user: user,
				comment: "comment",
				stupidityId: stupidity.id,
				fixId: createdFix.id
			    });
			}).then(() => {
			    setTimeout(() => {
				assert.fail('Should have failed');
				done();
			    });
			}).catch(error => {
			    setTimeout(() => {
				assert.equal(error.code, 401);
				done();
			    });
			});
		    });
		});

		it('should 401 on trying to vote for your own fix', done => {
		    Stupidity.findOne().then(stupidity => {
			var ids = {
			    user: user,
			    stupidityId: stupidity.id
			};
			var newFix = {
			    image: testImage,
			    comment: "New Comment"
			};

			var createdFix = {};
			fixingService.create(_.clone(newFix), ids).then(fix => {
			    setTimeout(() => {
				assert.equal(fix.submitterId, user.id);
			    });
			    createdFix = fix;
			    return fixingVotingService.get(5, {
				user: user,
				comment: "comment",
				stupidityId: stupidity.id,
				fixId: createdFix.id
			    });
			}).then(() => {
			    setTimeout(() => {
				assert.fail('Should have failed');
				done();
			    });
			}).catch(error => {
			    setTimeout(() => {
				assert.equal(error.code, 401);
				done();
			    });
			});
		    });
		});

		it('should 401 on trying to vote for closed fix', done => {
		    Stupidity.findOne().then(stupidity => {
			var ids = {
			    user: users[2],
			    stupidityId: stupidity.id
			};
			var newFix = {
			    image: testImage,
			    comment: "New Comment"
			};

			var createdFix = {};
			fixingService.create(_.clone(newFix), ids).then(fix => {
			    createdFix = fix;
			    setTimeout(() => {
				assert.notEqual(createdFix.submitterId, users[0].id);
			    });
			    users[0].credibility.hits = 100;

			    return fixingVotingService.get(5, {
				user: users[0],
				comment: "comment",
				stupidityId: stupidity.id,
				fixId: createdFix.id
			    });
			}).then(() => {
			    setTimeout(() => {
				assert.notEqual(createdFix.submitterId, users[1].id);
			    });
			    return fixingVotingService.get(5, {
				user: users[1],
				comment: "comment",
				stupidityId: stupidity.id,
				fixId: createdFix.id
			    });
			}).then(() => {
			    setTimeout(() => {
				assert.fail('Should have failed');
				done();
			    });
			}).catch(error => {
			    setTimeout(() => {
				assert.equal(error.code, 401);
				done();
			    });
			});
		    });
		});
	    });

	    describe('fixing', () => {
		describe('should 400 on invalid data',  () => {
		    it('invalid image', done => {
			Stupidity.findOne().then(stupidity => {
			    var ids = {
				user: user,
				stupidityId: stupidity.id
			    };
			    var newFix = {
				image: 'totally not a valid image',
				comment: "New Comment"
			    };

			    fixingService.create(newFix, ids).then(() => {
				setTimeout(() => {
				    assert.fail('Should have failed');
				    done();
				});
			    }).catch(error => {
				setTimeout(() => {
				    assert.property(error, "code");
				    assert.equal(error.code, 400);

				    done();
				});
			    });
			});
		    });
		});

		it('should 404 for non-existing stupidity', done => {
		    var idToRequest = "012345678901234567890123";

		    assert.isOk(validators.objectId(idToRequest), 'id is valid');

		    var ids = {
			user: user,
			stupidityId: idToRequest
		    };

		    var newFix = {
			image: testImage,
			comment: "New Comment"
		    };

		    fixingService.create(newFix, ids).then(() => {
			assert.fail('Should have failed');
			done();
		    }).catch(error => {
			assert.property(error, "code");
			assert.equal(error.code, 404);

			done();
		    });
		});

		it('should 403 for unauthorized or banned user', done => {
		    Stupidity.findOne().then(stupidity => {
			var ids = {
			    user: user,
			    stupidityId: stupidity.id
			};
			var newFix = {
			    image: testImage,
			    comment: "New Comment"
			};

			user.isBanned = true;

			fixingService.create(newFix, ids).then(() => {
			    user.isBanned = false;
			    setTimeout(() => {
				assert.fail('Should have failed');

				done();
			    });
			}).catch(error => {
			    user.isBanned = false;
			    setTimeout(() => {
				assert.property(error, "code");
				assert.equal(error.code, 403);

				done();
			    });
			});
		    });
		});

		it('should 401 on trying to send fix twice', done => {
		    Stupidity.findOne().then(stupidity => {
			var ids = {
			    user: user,
			    stupidityId: stupidity.id
			};
			var newFix = {
			    image: testImage,
			    comment: "New Comment"
			};

			fixingService.create(_.clone(newFix), ids).then(() => {
			    fixingService.create(_.clone(newFix), ids).then(() => {
				setTimeout(() => {
				    assert.fail('Should have failed');
				    done();
				});
			    }).catch(error => {
				setTimeout(() => {
				    assert.property(error, "code");
				    assert.equal(error.code, 401);
				    done();
				});
			    });
			}).catch(error => {
			    setTimeout(() => {
				assert.fail('Should have worked');
				done();
			    });
			});
		    });
		});

		it('should 401 on trying to fix the fixed stupidity', done => {
		    Stupidity.findOne().then(stupidity => {
			var ids = {
			    user: user,
			    stupidityId: stupidity.id
			};
			var newFix = {
			    image: testImage,
			    comment: "New Comment"
			};

			fixingService.create(_.clone(newFix), ids).then(fixId => {
			    return Fix.findOne({_id: fixId});
			}).then(fix => {
			    fix.isApproved = true;

			    return fix.save();
			}).then(fix => {
			    assert.isOk(fix.isApproved);
			    
			    fixingService.create(_.clone(newFix), ids).then(() => {
				setTimeout(() => {
				    assert.fail('Should have failed');
				    done();
				});
			    }).catch(error => {
				setTimeout(() => {
				    assert.property(error, "code");
				    assert.equal(error.code, 401);
				    done();
				});
			    });
			}).catch(error => {
			    setTimeout(() => {
				assert.fail('Should have worked');
				done();
			    });
			});
		    });
		});
	    });
	});

	describe('voting', () => {
	    it('should be able to vote for fix', done => {
		Stupidity.findOne().then(stupidity => {
		    var ids = {
			user: users[1],
			stupidityId: stupidity.id
		    };
		    var newFix = {
			image: testImage,
			comment: "New Comment"
		    };

		    fixingService.create(_.clone(newFix), ids).then(fix => {
			return fixingVotingService.get(5, {
			    user: user,
			    comment: "comment",
			    stupidityId: stupidity.id,
			    fixId: fix.id
			});
		    }).then(() => {
			return Fix.findOne();
		    }).then(onlyFix => {
			setTimeout(() => {
			    assert.notEqual(onlyFix.voteCount, 0, 'should have added a vote');
			    done();
			});
		    }).catch(error => {
			setTimeout(() => {
			    assert.fail('Should have worked');
			    done();
			});
		    });
		});
	    });
	});

	describe('moderation', () => {
	    it('should yield one random fix', done => {
		Stupidity.findOne().then(stupidity => {
		    var newFixes = _.map(users, user => {
			var ids = {
			    user: user,
			    stupidityId: stupidity.id
			};
			var newFix = {
			    image: testImage,
			    comment: "New Comment"
			};

			return fixingService.create(newFix, ids);
		    });

		    Promise.all(newFixes).then(createdFixes => {
			debugger;
			return fixingService.find({user: user});
		    }).then(fix => {
			setTimeout(() => {
			    assert.isArray(fix, "is an object");
			    assert.equal(fix.length, 1, "should have one fix");
			    done();
			});
		    }).catch(error => {
			debugger;
			setTimeout(() => {
			    assert.fail('Should have worked');
			    done();
			});
		    });
		});
	    });

	    it('should yield fixes only by other users', done => {
		Stupidity.findOne().then(stupidity => {
		    var twoUsers = [users[0], users[1]];
		    var ids = _.map(twoUsers, user => {
			return {
			    user: user,
			    stupidityId: stupidity.id
			};
		    });

		    var newFixes = _.map(ids, userIds => {
			var newFix = {
			    image: testImage,
			    comment: userIds.user.id
			};

			return fixingService.create(_.clone(newFix), userIds);
		    });

		    var thisUser = twoUsers[0];
		    var otherUser = twoUsers[1];

		    Promise.all(newFixes).then(createdFixes => {
			var fixes = _.map(_.range(25), () => {
			    return fixingService.find({user: user});
			});

			return Promise.all(fixes);
		    }).then(fixes => {
			setTimeout(() => {
			    _.map(fixes, fix => {
				assert.isArray(fix, "is an object");
				assert.equal(fix.length, 1, "should have one fix");
				assert.equal(fix[0].comment, otherUser.id, "should be by the other user");
			    });

			    done();
			});
		    }).catch(error => {
			setTimeout(() => {
			    assert.fail('Should have worked');
			    done();
			});
		    });
		});
	    });

	    it('should yield [] when no fixes are available', done => {
		Fix.count().then(count => {
		    setTimeout(() => {
			assert.equal(count, 0, "no fixes submitted");
		    });

		    return fixingService.find({user: user});
		}).then(fix => {
		    setTimeout(() => {
			assert.isArray(fix, "is an array");
			assert.equal(fix.length, 0, "should be empty");
			done();
		    });
		}).catch(error => {
		    setTimeout(() => {
			assert.fail('Should have worked');
			done();
		    });
		});
	    });

	    it('should yield only un-voted fixes', done => {
		Stupidity.findOne().then(stupidity => {
		    var ids = {
			user: users[1],
			stupidityId: stupidity.id
		    };
		    var newFix = {
			image: testImage,
			comment: "New Comment"
		    };

		    fixingService.create(_.clone(newFix), ids).then(fix => {
			return fixingVotingService.get(5, {
			    user: user,
			    comment: "comment",
			    stupidityId: stupidity.id,
			    fixId: fix.id
			});
		    }).then(() => {
			return fixingService.find({user: user});
		    }).then(fix => {
			setTimeout(() => {
			    assert.isArray(fix, "is an array");
			    assert.equal(fix.length, 0, "should be empty");
			    done();
			});
		    }).catch(error => {
			setTimeout(() => {
			    assert.fail('Should have worked');
			    done();
			});
		    });
		});
	    });

	    it('should auto-verdict on vote threshold', done => {
		Stupidity.findOne().then(stupidity => {
		    var ids = {
			user: users[1],
			stupidityId: stupidity.id
		    };
		    var newFix = {
			image: testImage,
			comment: "New Comment"
		    };

		    fixingService.create(_.clone(newFix), ids).then(fix => {
			// make sure that we are so credible we one-shot everything
			user.credibility.hits = 100;

			return fixingVotingService.get(5, {
			    user: user,
			    comment: "comment",
			    stupidityId: stupidity.id,
			    fixId: fix.id
			});
		    }).then(() => {
			return Fix.findOne({submitterId: users[1].id});
		    }).then(fix => {
			setTimeout(() => {
			    assert.isOk(fix.hasVerdict, "should have a verdict");
			    done();
			});
		    }).catch(error => {
			setTimeout(() => {
			    assert.fail('Should have worked');
			    done();
			});
		    });
		});
	    });
	});

	describe('user', () => {
	    describe('votes', () => {
		it('should be given for the accepted fix', done => {
		    Stupidity.findOne().then(stupidity => {
			var ids = {
			    user: users[1],
			    stupidityId: stupidity.id
			};
			var newFix = {
			    image: testImage,
			    comment: "New Comment"
			};

			fixingService.create(_.clone(newFix), ids).then(fix => {
			    // make sure that we are so credible we one-shot everything
			    user.credibility.hits = 100;

			    return fixingVotingService.get(5, {
				user: user,
				comment: "comment",
				stupidityId: stupidity.id,
				fixId: fix.id
			    });
			}).then(() => {
			    return Fix.findOne({submitterId: users[1].id});
			}).then(fix => {
			    setTimeout(() => {
				assert.isOk(fix.hasVerdict, "should have a verdict");
			    });

			    return User.findOne({_id: user._id});
			}).then(user => {
			    setTimeout(() => {
				assert.notEqual(user.votes.bought, 0, "should have been awarded");
				done();
			    });
			}).catch(error => {
			    setTimeout(() => {
				assert.fail('Should have worked');
				done();
			    });
			});
		    });
		});

		it('should be given for complementary verdict on fix', done => {
		    Stupidity.findOne().then(stupidity => {
			var creator = users[1];
			var ids = {
			    user: creator,
			    stupidityId: stupidity.id
			};
			var newFix = {
			    image: testImage,
			    comment: "New Comment"
			};
			var voter = users[2];
			var lasthitter = users[3];
			lasthitter.credibility.hits = 100;

			var createdFix = {};
			fixingService.create(_.clone(newFix), ids).then(fix => {
			    createdFix = fix;
			    return fixingVotingService.get(5, {
				user: voter,
				comment: "comment",
				stupidityId: stupidity.id,
				fixId: createdFix.id
			    });
			}).then(() => {
			    return Fix.findOne({submitterId: creator.id});
			}).then(fix => {
			    setTimeout(() => {
				assert.isNotOk(fix.hasVerdict, "shouldn't have a verdict yet");
			    });
			}).then(() => {
			    return fixingVotingService.get(5, {
				user: lasthitter,
				comment: "comment",
				stupidityId: stupidity.id,
				fixId: createdFix.id
			    });
			}).then(() => {
			    return Fix.findOne({submitterId: creator.id});
			}).then(fix => {
			    setTimeout(() => {
				assert.isOk(fix.hasVerdict, "should have a verdict");
			    });

			    return User.findOne({_id: voter._id});
			}).then(user => {
			    setTimeout(() => {
				assert.notEqual(user.votes.bought, 0, "should have been awarded");
				done();
			    });
			}).catch(error => {
			    setTimeout(() => {
				assert.fail('Should have worked');
				done();
			    });
			});
		    });
		});
	    });

	    describe('credibility', () => {
		it('should increase on complementary verdict', done => {
		    Stupidity.findOne().then(stupidity => {
			var creator = users[1];
			var ids = {
			    user: creator,
			    stupidityId: stupidity.id
			};
			var newFix = {
			    image: testImage,
			    comment: "New Comment"
			};
			var voter = users[2];
			var lasthitter = users[3];
			lasthitter.credibility.hits = 100;

			var createdFix = {};
			fixingService.create(_.clone(newFix), ids).then(fix => {
			    createdFix = fix;
			    return fixingVotingService.get(5, {
				user: voter,
				comment: "comment",
				stupidityId: stupidity.id,
				fixId: createdFix.id
			    });
			}).then(() => {
			    return Fix.findOne({submitterId: creator.id});
			}).then(fix => {
			    setTimeout(() => {
				assert.isNotOk(fix.hasVerdict, "shouldn't have a verdict yet");
			    });
			}).then(() => {
			    return fixingVotingService.get(5, {
				user: lasthitter,
				comment: "comment",
				stupidityId: stupidity.id,
				fixId: createdFix.id
			    });
			}).then(() => {
			    return Fix.findOne({submitterId: creator.id});
			}).then(fix => {
			    setTimeout(() => {
				assert.isOk(fix.hasVerdict, "should have a verdict");
			    });

			    return User.findOne({_id: voter._id});
			}).then(user => {
			    setTimeout(() => {
				assert.notEqual(user.credibility.hits, 0, "should have been awarded");
				done();
			    });
			}).catch(error => {
			    setTimeout(() => {
				assert.fail('Should have worked');
				done();
			    });
			});
		    });
		});

		it('should decrease on non-complementary verdict', done => {
		    Stupidity.findOne().then(stupidity => {
			var creator = users[1];
			var ids = {
			    user: creator,
			    stupidityId: stupidity.id
			};
			var newFix = {
			    image: testImage,
			    comment: "New Comment"
			};
			var voter = users[2];
			var lasthitter = users[3];
			lasthitter.credibility.hits = 100;

			var createdFix = {};
			fixingService.create(_.clone(newFix), ids).then(fix => {
			    createdFix = fix;
			    return fixingVotingService.get(0, {
				user: voter,
				comment: "comment",
				stupidityId: stupidity.id,
				fixId: createdFix.id
			    });
			}).then(() => {
			    return Fix.findOne({submitterId: creator.id});
			}).then(fix => {
			    setTimeout(() => {
				assert.isNotOk(fix.hasVerdict, "shouldn't have a verdict yet");
			    });
			}).then(() => {
			    return fixingVotingService.get(5, {
				user: lasthitter,
				comment: "comment",
				stupidityId: stupidity.id,
				fixId: createdFix.id
			    });
			}).then(() => {
			    return Fix.findOne({submitterId: creator.id});
			}).then(fix => {
			    setTimeout(() => {
				assert.isOk(fix.hasVerdict, "should have a verdict");
			    });

			    return User.findOne({_id: voter._id});
			}).then(user => {
			    setTimeout(() => {
				assert.notEqual(user.credibility.misses, 0, "should have been awarded");
				done();
			    });
			}).catch(error => {
			    setTimeout(() => {
				assert.fail('Should have worked');
				done();
			    });
			});
		    });
		});

		it('should increase for accepted fixes', done => {
		    Stupidity.findOne().then(stupidity => {
			var ids = {
			    user: users[1],
			    stupidityId: stupidity.id
			};
			var newFix = {
			    image: testImage,
			    comment: "New Comment"
			};

			fixingService.create(_.clone(newFix), ids).then(fix => {
			    // make sure that we are so credible we one-shot everything
			    user.credibility.hits = 100;

			    return fixingVotingService.get(5, {
				user: user,
				comment: "comment",
				stupidityId: stupidity.id,
				fixId: fix.id
			    });
			}).then(() => {
			    return Fix.findOne({submitterId: users[1].id});
			}).then(fix => {
			    setTimeout(() => {
				assert.isOk(fix.hasVerdict, "should have a verdict");
			    });

			    return User.findOne({_id: user._id});
			}).then(user => {
			    setTimeout(() => {
				assert.notEqual(user.credibility.hits, 0, "should have been awarded");
				done();
			    });
			}).catch(error => {
			    setTimeout(() => {
				assert.fail('Should have worked');
				done();
			    });
			});
		    });
		});

		it('should decrease for rejected fixes', done => {
		    Stupidity.findOne().then(stupidity => {
			var ids = {
			    user: users[1],
			    stupidityId: stupidity.id
			};
			var newFix = {
			    image: testImage,
			    comment: "New Comment"
			};

			fixingService.create(_.clone(newFix), ids).then(fix => {
			    // make sure that we are so credible we one-shot everything
			    user.credibility.hits = 100;

			    return fixingVotingService.get(0, {
				user: user,
				comment: "comment",
				stupidityId: stupidity.id,
				fixId: fix.id
			    });
			}).then(() => {
			    return Fix.findOne({submitterId: users[1].id});
			}).then(fix => {
			    setTimeout(() => {
				assert.isOk(fix.hasVerdict, "should have a verdict");
			    });

			    return User.findOne({_id: users[1]._id});
			}).then(user => {
			    setTimeout(() => {
				assert.notEqual(user.credibility.misses, 0, "should have been awarded");
				done();
			    });
			}).catch(error => {
			    setTimeout(() => {
				assert.fail('Should have worked');
				done();
			    });
			});
		    });
		});
	    });
	});
    });
});
