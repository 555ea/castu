var User = require('../api/models/user');
var Stupidity = require('../api/models/stupidity');

var assert = require('chai').assert;
var mongoose = require('mongoose');
var _ = require('lodash');

var apiServer = require('../servers/apiServer')();
var userService = apiServer.service('/user');
var stupidityService = apiServer.service('/stupidities');
var fixingService = apiServer.service('/stupidities/:stupidityId/fix');
var reportingService = apiServer.service('/stupidities/:stupidityId/report');

var stupidityCount = 10;

var testImage = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAAEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKAP/2Q==';

describe('Users', () => {
    var users = {};

    beforeEach(done => {
	users = _.map(_.range(10), () => {return new User();});

	Promise.all(_.map(users, user => {
	    return user.save();
	})).then(users => {
	    var user = users[0];

	    Promise.all(
		_.map(
		    _.range(stupidityCount),
		    (i) => {
			var stupidity = new Stupidity({
			    location: [i, i],
			    severity: 0,
			    category: 0,
			    image: testImage,
			    submitterId: user._id
			});

			// optional fields 
			if(i % 2) {
			    stupidity.comment = "lorem ipsum";
			}

			return stupidity.save();
		    }
		)
	    ).then(() => {
		User.count().then(count => {
		    done();
		});
	    });
	}).catch(done);
    });

    after(done => {
	mongoose.connection.db.dropDatabase().then(() => {
	    done();
	});
    });

    describe('user', () => {
	it('should receive own stupidities, fixes and reports', done => {
	    // all the stupidities were made by user 0

	    userService.find({user: users[0]}).then(user => {
		setTimeout(() => {
		    assert.property(user, 'stupidities', 'should have stupidities');
		    assert.equal(user.stupidities.length, stupidityCount, 'all stupidities');
		    assert.property(user, 'reports', 'should have reports');
		    assert.equal(user.reports.length, 0, 'no reports');
		    assert.property(user, 'fixes', 'should have fixes');
		    assert.equal(user.fixes.length, 0, 'no fixes');
		});

		var params = {
		    user: users[1],
		    stupidityId: user.stupidities[0].id
		};
		var newReport = {
		    comment: "New Comment"
		};
		var newFix = {
		    image: testImage,
		    comment: "New Comment"
		};

		return Promise.all([
		    reportingService.create(_.clone(newReport), params),
		    fixingService.create(_.clone(newFix), params)
		]);
	    }).then(() => {
		return userService.find({user: users[1]});
	    }).then(user => {
		setTimeout(() => {
		    assert.equal(user.reports.length, 1, 'one report');
		    assert.equal(user.fixes.length, 1, 'one fix');
		    done();
		});
	    });
	});
    });

    describe('admin', () => {
	it('access any user profile', done => {
	    var admin = users[0];
	    var user = users[1];

	    admin.isAdmin = true;
	    
	    userService.get(user.id, {user: admin}).then(gotUser => {
		setTimeout(() => {
		    assert.equal(gotUser.id, user.id, "should be the same user");
		    done();
		});
	    }).catch(done);
	});

	it('ban and unban user', done => {
	    var admin = users[0];
	    var bannedUser = users[1];

	    admin.isAdmin = true;
	    
	    userService.remove(bannedUser.id, {user: admin, action: 'ban'}).then(user => {
		return User.findOne({_id: user.id});
	    }).then(user  => {
		setTimeout(() => {
		    assert.isOk(user.isBanned, "should be banned");
		});
		return userService.remove(user.id, {user: admin, action: 'unban'});
	    }).then(user => {
		return User.findOne({_id: user.id});
	    }).then(user => {
		setTimeout(() => {
		    assert.isNotOk(user.isBanned, "should be unbanned");
		    done();
		});
	    }).catch(done);
	});
    });
});
