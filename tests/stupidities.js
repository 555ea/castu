var assert = require('chai').assert;
var mongoose = require('mongoose');
var _ = require('lodash');

var apiServer = require('../servers/apiServer')();
var stupidityService = apiServer.service('/stupidities');

var Stupidity = require('../api/models/stupidity');
var User = require('../api/models/user');

var validators = require('../api/helpers/validators');

var stupidityCount = 10;

var testImage = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAAEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKAP/2Q==';

var connection = mongoose.connect('localhost', 'stupidityTests');

describe('Stupididies', () => {
    var user = new User();
    var admin = new User();

    var checkFields = (stupidity) => {
	// avoids all the mongoose metaprogramming bonkery
	stupidity = stupidity.toJSON();

	assert.property(stupidity, '_id');
	assert.property(stupidity, 'location');
	assert.property(stupidity, 'createdAt');
	assert.property(stupidity, 'updatedAt');
	assert.property(stupidity, 'image');
	assert.property(stupidity, 'voteCount');
	// comments are optional
	// assert.property(stupidity, 'comment');
    };

    before(done => {
	admin.save().then(() => {
	    admin.isAdmin = true;
	    return admin.save();
	}).then(() => {
	    return user.save();
	}).then(user => {
	    Promise.all(
		_.map(
		    _.range(stupidityCount),
		    (i) => {
			var stupidity = new Stupidity({
			    location: [i, i],
			    severity: 0,
			    category: 0,
			    image: "image_name",
			    submitterId: user._id
			});

			// optional fields 
			if(i % 2) {
			    stupidity.comment = "lorem ipsum";
			}

			return stupidity.save();
		    }
		)
	    ).then(() => done());
	}).catch(done);
    });

    after(() => {
	mongoose.connection.db.dropDatabase();
    });

    describe('creation', () => {
        it('should return all available stupidities', done => {
            stupidityService.find().then(stupidities => {
                assert.lengthOf(stupidities, stupidityCount, 'has all the created stupidities');

                done();
            }).catch(done);
        });

        it('should return stupidity by its ID', done => {
            stupidityService.find().then(stupidities => {
                var idToRequest = stupidities[0]._id;

                stupidityService.get(idToRequest).then(stupidity => {
                    assert.equal(String(stupidity._id), String(idToRequest), 'IDs match');

                    done();
                }).catch(done);
            }).catch(done);
        });

        describe('stupidity has all the required fields', () => {
            it("all the stupidities", done => {
                stupidityService.find().then(stupidities => {
                    var stupidityChecked = _.after(stupidities.length, () => done());

                    _.each(stupidities, stupidity => {
                        checkFields(stupidity);
                        stupidityChecked();
                    });
                }).catch(done);
            });

            it("get by ID", done => {
                stupidityService.find().then(stupidities => {
                    var idToRequest = stupidities[0]._id;

                    stupidityService.get(idToRequest).then(stupidity => {
                        checkFields(stupidity);
                        done();
                    }).catch(done);
                }).catch(done);
            });
        });

        it('get should return 404 for non-existant stupidity', done => {
	    var idToRequest = "012345678901234567890123";

	    assert.isOk(validators.objectId(idToRequest), 'id is valid');

	    stupidityService.get(idToRequest).then(stupidity => {
		setTimeout(() => {
		    assert.fail(undefined, stupidity, "service promise should throw error");
		    done();
		});
	    }).catch(error => {
		setTimeout(() => {
		    assert.property(error, "code");
		    assert.equal(error.code, 404);
		    done();
		});
	    });
        });

        it('get should return 404 for the invalid stupidity ID', done => {
            var idToRequest = "TOTALLY INVALID ID";
            
            assert.isNotOk(validators.objectId(idToRequest), 'id is invalid');

            stupidityService.get(idToRequest).then(stupidity => {
		setTimeout(() => {
		    assert.fail(undefined, stupidity, "service promise should throw error");
		    done();
		});
            }).catch(error => {
		setTimeout(() => {
		    assert.property(error, "code");
		    assert.equal(error.code, 404);
		    done();
		});
            });
        });

        it('should create stupidity', done => {
            var data = {
                location: [1, 2],
                image: testImage,
                voteCount: 9001,
		severity: 0,
		category: 0,
                comment: 'new stupidity!'
            };

            stupidityService.create(data, {user: user}).then(stupidity => {
                stupidityService.get(stupidity.id).then(stupidity => {
                    checkFields(stupidity);

                    assert.equal(stupidity.comment, data.comment);
                    assert.deepEqual(stupidity.location, data.location);
                    assert.equal(stupidity.voteCount, 0, 'vote count defaults to 0');
                    assert.notEqual(stupidity.image, data.image, 'image is converted to its new path');

                    done();
                }).catch(done);
            }).catch(done);
        });

        describe('should fail to create stupidity with', () => {
            it('no severity', done => {
                var data = {
                    location: [1, 2],
                    image: testImage,
		    category: 0
                };

                stupidityService.create(data, {user: user}).then(id => {
		    setTimeout(() => {
			assert.fail(undefined, '', "creating stupidities with no submitter");
			done();
		    });
                }).catch(error => {
		    setTimeout(() => {
			assert.property(error, "code");
			assert.equal(error.code, 400);
			done();
		    });
                });
	    });

            it('no category', done => {
                var data = {
                    location: [1, 2],
                    image: testImage,
		    severity: 0
                };

                stupidityService.create(data, {user: user}).then(id => {
		    setTimeout(() => {
			assert.fail(undefined, '', "creating stupidities with no submitter");
			done();
		    });
                }).catch(error => {
		    setTimeout(() => {
			assert.property(error, "code");
			assert.equal(error.code, 400);
			done();
		    });
                });
	    });

            it('images of the wrong format', done => {
                var data = {
                    location: [1, 2],
		    severity: 0,
		    category: 0,
                    image: "Totally not an image"
                };

                stupidityService.create(data, {user: user}).then(id => {
		    assert.fail(undefined, '', "creating stupidities with invalid images");
                    done();
                }).catch(error => {
		    setTimeout(() => {
			assert.property(error, "code");
			assert.property(error, "message");
			assert.equal(error.code, 400);
			assert.include(error.message, "image");
			done();
		    });
                });
            });
        });
    });

    describe('deletion', () => {
	it('should be allowed to delete own recent stupidities', done => {
            var data = {
                location: [1, 2],
                image: testImage,
                voteCount: 9001,
		severity: 0,
		category: 0,
                comment: 'new stupidity!'
            };

	    var stupidityId;
            stupidityService.create(data, {user: user}).then(stupidity => {
		stupidityId = stupidity.id;
                return stupidityService.get(stupidityId);
	    }).then(stupidity => {
		setTimeout(() => {
		    assert.isOk(!stupidity.isHidden, 'should not be deleted');
		});
		return stupidityService.remove(stupidityId, {user: user});
	    }).then(() => {
		return Stupidity.findOne({_id: stupidityId});
	    }).then(stupidity => {
		setTimeout(() => {
		    assert.isOk(stupidity.isHidden, 'should be deleted');
		    done();
		});
            }).catch(done);
	});
    });

    describe('admin', () => {
	it('should be allowed to delete stupidities', done => {
            var data = {
                location: [1, 2],
                image: testImage,
                voteCount: 9001,
		severity: 0,
		category: 0,
                comment: 'new stupidity!'
            };

	    var stupidityId;
            stupidityService.create(data, {user: user}).then(stupidity => {
		stupidityId = stupidity.id;
                return stupidityService.get(stupidityId);
	    }).then(stupidity => {
		setTimeout(() => {
		    assert.isOk(!stupidity.isHidden, 'should not be deleted');
		    assert.notEqual(stupidity.submitterId, admin.id, 'should not be made by admin');
		});
		return stupidityService.remove(stupidityId, {user: admin});
	    }).then(() => {
		return Stupidity.findOne({_id: stupidityId});
	    }).then(stupidity => {
		setTimeout(() => {
		    assert.isOk(stupidity.isHidden, 'should be deleted');
		    done();
		});
            }).catch(done);
	});

	it('should be allowed to edit stupidities', done => {
	    var updates = {
		location: [45, 45],
		severity: 1,
		category: 1
	    };

	    var data = {
		location: [1, 2],
		image: testImage,
		severity: 0,
		category: 0,
		comment: 'new stupidity!'
	    };

	    var stupidityId;
	    stupidityService.create(data, {user: user}).then(stupidity => {
		stupidityId = stupidity.id;
		stupidity = stupidity.toObject();

		setTimeout(() => {
		    _.forOwn(updates, (value, key) => {
			assert.notEqual(stupidity[key], value, 'should not be updated');
		    });
		});

		return stupidityService.update(stupidityId, _.cloneDeep(updates), {user: admin});
	    }).then(() => {
		return Stupidity.findOne({_id: stupidityId});
	    }).then(stupidity => {
		stupidity = stupidity.toObject();
		setTimeout(() => {
		    _.forOwn(updates, (value, key) => {
			if(value instanceof Array) {
			    assert.sameMembers(stupidity[key], value, 'should not be updated');
			} else {
			    assert.equal(stupidity[key], value, 'should not be updated');
			}
		    });
		    done();
		});
	    }).catch(done);
	});
    });
});
