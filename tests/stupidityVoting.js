var assert = require('chai').assert;
var mongoose = require('mongoose');
var _ = require('lodash');

var apiServer = require('../servers/apiServer')();
var stupidityService = apiServer.service('/stupidities');
var votingService = apiServer.service('/stupidities/:stupidityId/vote');

var Stupidity = require('../api/models/stupidity');
var User = require('../api/models/user');

var validators = require('../api/helpers/validators');

var stupidityCount = 10;

describe('Stupidities', () => {
    var user = new User();

    before(done => {
	user.save().then(newUser => {
	    user = newUser;

	    Promise.all(
		_.map(
		    _.range(stupidityCount),
		    (i) => {
			var stupidity = new Stupidity({
			    location: [i, i],
			    severity: 0,
			    category: 0,
			    image: "image_name",
			    submitterId: user._id
			});

			// optional fields 
			if(i % 2) {
			    stupidity.comment = "lorem ipsum";
			}

			return stupidity.save();
		    }
		)
	    ).then(() => done());
	}).catch(done);
    });

    after(() => {
	mongoose.connection.db.dropDatabase();
    });

    describe('voting', () => {
	describe('data validation', () => {
	    it('should 400 on no voteCount or voteCount <= 0', done => {
		stupidityService.find().then(stupidities => {
		    var idToRequest = stupidities[0]._id;

		    votingService.get(0, {user: user, stupidityId: idToRequest}).then(voteCount => {
			assert.fail(undefined, stupidity, "service promise should throw error");
			done();
		    }).catch(error => {
			assert.property(error, "code");
			assert.equal(error.code, 400);

			return votingService.get(-666, {stupidityId: idToRequest});
		    }).catch(error => {
			assert.property(error, "code");
			assert.equal(error.code, 400);

			done();
		    }).catch(done);

		}).catch(done);
	    });

	    it('should 404 for non-existing stupidity', done => {
		var idToRequest = "012345678901234567890123";

		assert.isOk(validators.objectId(idToRequest), 'id is valid');

		votingService.get(1, {user: user, stupidityId: idToRequest}).then(stupidity => {
		    assert.fail(undefined, stupidity, "service promise should throw error");
		    done();
		}).catch(error => {
		    assert.property(error, "code");
		    assert.equal(error.code, 404);
		    done();
		});
	    });

	    it('should 403 for unauthorized users', done => {
		stupidityService.find().then(stupidities => {
		    var idToRequest = stupidities[0]._id;

		    votingService.get(1, {stupidityId: idToRequest}).then(voteCount => {
			assert.fail(undefined, stupidity, "service promise should throw error");
			done();
		    }).catch(error => {
			assert.property(error, "code");
			assert.equal(error.code, 403);
			done();
		    }).catch(done);
		}).catch(done);
	    });

	    it('should 403 for banned users', done => {
		user.isBanned = true;

		stupidityService.find().then(stupidities => {
		    var idToRequest = stupidities[0]._id;

		    votingService.get(1, {user: user, stupidityId: idToRequest}).then(voteCount => {
			user.isBanned = false;
			
			assert.fail(undefined, stupidity, "service promise should throw error");
			done();
		    }).catch(error => {
			user.isBanned = false;
			
			assert.property(error, "code");
			assert.equal(error.code, 403);
			done();
		    }).catch(done);
		}).catch(done);
	    });
	});

	describe('giving 1 vote', () => {
	    it('should work having N>0 additional votes', done => {
		user.votesLeft = 10;

		stupidityService.find().then(stupidities => {
		    var idToRequest = stupidities[0]._id;

		    votingService.get(1, {user: user, stupidityId: idToRequest}).then(voteCount => {
			done();
		    }).catch(done);
		}).catch(done);
	    });

	    it('should work having 0 additional votes', done => {
		user.votesLeft = 0;

		stupidityService.find().then(stupidities => {
		    // TODO: make sure we didn't vote for this stupidity yet
		    var idToRequest = stupidities[1]._id;

		    votingService.get(1, {user: user, stupidityId: idToRequest}).then(voteCount => {
			done();
		    }).catch(done);
		}).catch(done);
	    });
	});
	
	describe('multi-voting', () => {
	    it('should give N votes', done => {
		user.votesLeft = 10;

		stupidityService.find().then(stupidities => {
		    // TODO: make sure we didn't vote for this stupidity yet
		    var idToRequest = stupidities[1]._id;
		    var votesToAdd = 3;
		    var expectedVoteCount = stupidities[1].voteCount + votesToAdd;

		    assert.isOk(user.votesLeft > votesToAdd, 'have enough votes');

		    votingService.get(votesToAdd, {user: user, stupidityId: idToRequest}).then(voteCount => {
			assert.equal(voteCount, expectedVoteCount);
			done();
		    }).catch(done);
		}).catch(done);
	    });

	    it('should fail with 400 to give >1 votes when no votes left', done => {
		user.votesLeft = 0;

		stupidityService.find().then(stupidities => {
		    // TODO: make sure we didn't vote for this stupidity yet
		    var idToRequest = stupidities[4]._id;
		    var votesToAdd = user.votesLeft + 10;
		    var expectedVoteCount = stupidities[4].voteCount + votesToAdd;

		    assert.isOk(user.votesLeft < votesToAdd, 'have not enough votes');

		    votingService.get(votesToAdd, {user: user, stupidityId: idToRequest}).then(voteCount => {
			assert.fail(undefined, stupidity, "service promise should throw error");
			done();
		    }).catch(error => {
			assert.property(error, "code");
			assert.equal(error.code, 400);
		    }).then(() => {
			votingService.get(1, {user: user, stupidityId: idToRequest}).then(voteCount => {
			    done();
			}).catch(() => {
			    assert.fail(undefined, stupidity, "should've given 1 vote anyway");
			    done();
			});
		    }).catch(done);
		}).catch(done);
	    });

	    it('should fail with 400 to give more votes than votes left', done => {
		user.votesLeft = 10;

		stupidityService.find().then(stupidities => {
		    // TODO: make sure we didn't vote for this stupidity yet
		    var idToRequest = stupidities[4]._id;
		    var votesToAdd = 20;
		    var expectedVoteCount = stupidities[4].voteCount + votesToAdd;

		    assert.isOk(user.votesLeft < votesToAdd, 'have not enough votes');

		    votingService.get(votesToAdd, {user: user, stupidityId: idToRequest}).then(voteCount => {
			assert.fail(undefined, stupidity, "service promise should throw error");
			done();
		    }).catch(error => {
			assert.property(error, "code");
			assert.equal(error.code, 400);
			done();
		    }).catch(done);
		}).catch(done);
	    });
	});
	
	describe('reporting', () => {
	    it('should return vote count correctly and idempotently', done => {
		stupidityService.find().then(stupidities => {
		    var idToRequest = stupidities[0]._id;
		    var expectedVoteCount = stupidities[0].voteCount;

		    votingService.find({stupidityId: idToRequest}).then(voteCount => {
			// correctly
			assert.equal(voteCount, expectedVoteCount);
			return votingService.find({stupidityId: idToRequest});
		    }).then(voteCount => {
			// idempotently
			assert.equal(voteCount, expectedVoteCount);
			done();
		    }).catch(done);
		}).catch(done);
	    });

	    it('should update vote count correctly', done => {
		user.votesLeft = 10;
		
		stupidityService.find().then(stupidities => {
		    var idToRequest = stupidities[0]._id;
		    var votesToAdd = 2;
		    var expectedVoteCount = stupidities[0].voteCount + votesToAdd;

		    assert.isOk(user.votesLeft >= votesToAdd, 'have enough votes');

		    votingService.get(votesToAdd, {user: user, stupidityId: idToRequest}).then(voteCount => {
			assert.equal(voteCount, expectedVoteCount);
			return votingService.find({stupidityId: idToRequest});
		    }).then(voteCount => {
			assert.equal(voteCount, expectedVoteCount);
			done();
		    }).catch(done);
		}).catch(done);
	    });
	});

	describe('user', () => {
	    describe('vote count update', () => {
		it('should change vote count', done => {
		    user.votes.bought = 10;
		    var votesBefore = user.votesLeft;
		    var votesToAdd = 5;

		    assert.isOk(user.votesLeft >= votesToAdd, 'have enough votes');

		    stupidityService.find().then(stupidities => {
			var idToRequest = stupidities[0]._id;

			votingService.get(votesToAdd, {
			    user: user,
			    stupidityId: idToRequest
			}).then(voteCount => {
			    assert.equal(user.votesLeft, votesBefore - votesToAdd);
			    done();
			}).catch(done);
		    }).catch(done);
		});

		it('should change vote count when re-voting', done => {
		    var votesBefore = 10;
		    var votesToAdd = 5;
		    user.votes.bought = votesBefore;

		    stupidityService.find().then(stupidities => {
			var idToRequest = stupidities[0]._id;

			votingService.get(votesToAdd, {
			    user: user,
			    stupidityId: idToRequest
			}).then(voteCount => {
			    assert.equal(user.votesLeft, votesBefore - votesToAdd);

			    // voting for 1 is no longer free
			    votingService.get(1, {
				user: user,
				stupidityId: idToRequest
			    }).then(voteCount => {
				assert.equal(user.votesLeft, votesBefore - votesToAdd - 1);
				done();
			    });
			}).catch(done);
		    }).catch(done);
		});
	    });

	    describe('available vote reset', () => {
		it('should reset vote count when requested', done => {
		    var votesBefore = user.votes.free;
		    user.resetFreeVotes();
		    assert.notEqual(user.votes.free, votesBefore);
		    done();
		});

		it('should never go over the limit', done => {
		    user.votes.free = 50000;
		    user.resetFreeVotes();
		    assert.equal(user.votes.free, user.votes.freeCap);

		    user.votes.free = 0;
		    user.resetFreeVotes();
		    assert.equal(user.votes.free, user.votes.freeCap);
		    
		    done();
		});

		it('should not get influenced by bought votes', done => {
		    var boughtVotesToAdd = 9000;
		    user.votes.bought = boughtVotesToAdd;
		    assert.equal(user.votes.bought, boughtVotesToAdd);

		    user.votes.free = 50000;
		    user.resetFreeVotes();
		    assert.equal(user.votes.free, user.votes.freeCap);
		    assert.equal(user.votesLeft, user.votes.freeCap + boughtVotesToAdd);

		    user.votes.free = 0;
		    user.resetFreeVotes();
		    assert.equal(user.votes.free, user.votes.freeCap);
		    
		    done();
		});
	    });
	});
    });
});
	
