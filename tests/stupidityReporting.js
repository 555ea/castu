var assert = require('chai').assert;
var mongoose = require('mongoose');
var _ = require('lodash');

var apiServer = require('../servers/apiServer')();
var stupidityService = apiServer.service('/stupidities');
var reportingService = apiServer.service('/stupidities/:stupidityId/report');
var reportingVotingService = apiServer.service('/stupidities/:stupidityId/report/vote');

var Stupidity = require('../api/models/stupidity');
var User = require('../api/models/user');
var Report = require('../api/models/stupidityReport');
var ReportPart = require('../api/models/stupidityReportPart');

var validators = require('../api/helpers/validators');

var stupidityCount = 10;

describe('Stupidities', () => {
    var user = {};
    var users = {};

    beforeEach(done => {
	users = _.map(_.range(10), () => {return new User();});

	Promise.all(_.map(users, user => {
	    return user.save();
	})).then(users => {
	    user = users[0];

	    Promise.all(
		_.map(
		    _.range(stupidityCount),
		    (i) => {
			var testImage = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAAEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKAP/2Q==';
			var stupidity = new Stupidity({
			    location: [i, i],
			    severity: 0,
			    category: 0,
			    image: testImage,
			    submitterId: user._id
			});

			// optional fields 
			if(i % 2) {
			    stupidity.comment = "lorem ipsum";
			}

			return stupidity.save();
		    }
		)
	    ).then(() => {
		User.count().then(count => {
		    done();
		});
	    });
	}).catch(done);
    });

    afterEach(done => {
	mongoose.connection.db.dropDatabase().then(() => {
	    done();
	});
    });

    describe('reporting', () => {
	describe('errors', () => {
	    describe('voting', () => {
		it('should 403 for unauthorized or banned user', done => {
		    Stupidity.findOne().then(stupidity => {
			user.isBanned = true;

			var params = {
			    user: users[1],
			    stupidityId: stupidity.id
			};
			var newReport = {
			    comment: "New Comment"
			};

			reportingService.create(_.clone(newReport), params).then(report => {
			    setTimeout(() => {
				assert.notEqual(report.submitterId, user.id);
			    });

			    return reportingVotingService.create({
				vote: 'pro',
				comment: 'comment'
			    }, {
				user: user,
				comment: "comment",
				stupidityId: stupidity.id,
			    });
			}).then(() => {
			    setTimeout(() => {
				assert.fail('Should have failed');
				user.isBanned = false;
				done();
			    });
			}).catch(error => {
			    setTimeout(() => {
				assert.equal(error.code, 403);
				user.isBanned = false;
				done();
			    });
			});
		    });
		});

		it('should 401 on trying to vote twice', done => {
		    Stupidity.findOne().then(stupidity => {
			var params = {
			    user: users[1],
			    stupidityId: stupidity.id
			};
			var newReport = {
			    comment: "New Comment"
			};

			var createdReport = {};
			reportingService.create(_.clone(newReport), params).then(report => {
			    setTimeout(() => {
				assert.notEqual(report.submitterId, user.id);
			    });
			    createdReport = report;
			    return reportingVotingService.create({
				vote: 'pro',
				comment: 'comment'
			    }, {
				user: user,
				comment: "comment",
				stupidityId: stupidity.id,
			    });
			}).then(() => {
			    setTimeout(() => {
				assert.notEqual(createdReport.submitterId, user.id);
			    });
			    return reportingVotingService.create({
				vote: 'pro',
				comment: 'comment'
			    }, {
				user: user,
				comment: "comment",
				stupidityId: stupidity.id,
			    });
			}).then(() => {
			    setTimeout(() => {
				assert.fail('Should have failed');
				done();
			    });
			}).catch(error => {
			    setTimeout(() => {
				assert.equal(error.code, 401);
				done();
			    });
			});
		    });
		});

		it('should 401 on trying to vote for your own report', done => {
		    Stupidity.findOne().then(stupidity => {
			var params = {
			    user: user,
			    stupidityId: stupidity.id
			};
			var newReport = {
			    comment: "New Comment"
			};

			var createdReport = {};
			reportingService.create(_.clone(newReport), params).then(report => {
			    setTimeout(() => {
				assert.equal(report.submitterId, user.id);
			    });
			    createdReport = report;
			    return reportingVotingService.create({
				vote: 'pro',
				comment: 'comment'
			    }, {
				user: user,
				comment: "comment",
				stupidityId: stupidity.id,
			    });
			}).then(() => {
			    setTimeout(() => {
				assert.fail('Should have failed');
				done();
			    });
			}).catch(error => {
			    setTimeout(() => {
				assert.equal(error.code, 401);
				done();
			    });
			});
		    });
		});

		it('should 401 on trying to vote for closed report', done => {
		    Stupidity.findOne().then(stupidity => {
			var params = {
			    user: users[2],

			    stupidityId: stupidity.id
			};
			var newReport = {
			    comment: "New Comment"
			};

			var createdReport = {};
			reportingService.create(_.clone(newReport), params).then(report => {
			    createdReport = report;
			    setTimeout(() => {
				assert.notEqual(createdReport.submitterId, users[0].id);
			    });
			    users[0].credibility.hits = 100;

			    return reportingVotingService.create({
				vote: 'pro',
				comment: 'comment'
			    }, {
				user: users[0],
				comment: "comment",
				stupidityId: stupidity.id,
			    });
			}).then(() => {
			    setTimeout(() => {
				assert.notEqual(createdReport.submitterId, users[1].id);
			    });
			    return reportingVotingService.create({
				vote: 'pro',
				comment: 'comment'
			    }, {
				user: users[1],
				comment: "comment",
				stupidityId: stupidity.id,
			    });
			}).then(() => {
			    setTimeout(() => {
				assert.fail('Should have failed');
				done();
			    });
			}).catch(error => {
			    setTimeout(() => {
				assert.equal(error.code, 401);
				done();
			    });
			});
		    });
		});
	    });

	    describe('reporting', () => {
		it('should 404 for non-existing stupidity', done => {
		    var idToRequest = "012345678901234567890123";

		    assert.isOk(validators.objectId(idToRequest), 'id is valid');

		    var params = {
			user: user,
			stupidityId: idToRequest
		    };

		    var newReport = {
			comment: "New Comment"
		    };

		    reportingService.create(newReport, params).then(() => {
			setTimeout(() => {
			    assert.fail('Should have failed');
			    done();
			});
		    }).catch(error => {
			setTimeout(() => {
			    assert.property(error, "code");
			    assert.equal(error.code, 404);

			    done();
			});
		    });
		});

		it('should 403 for unauthorized or banned user', done => {
		    Stupidity.findOne().then(stupidity => {
			var params = {
			    user: user,
			    stupidityId: stupidity.id
			};
			var newReport = {
			    comment: "New Comment"
			};

			user.isBanned = true;

			reportingService.create(newReport, params).then(() => {
			    user.isBanned = false;
			    setTimeout(() => {
				assert.fail('Should have failed');

				done();
			    });
			}).catch(error => {
			    user.isBanned = false;
			    setTimeout(() => {
				assert.property(error, "code");
				assert.equal(error.code, 403);

				done();
			    });
			});
		    });
		});

		it('should 401 on trying to send report twice', done => {
		    Stupidity.findOne().then(stupidity => {
			var params = {
			    user: user,
			    stupidityId: stupidity.id
			};
			var newReport = {
			    comment: "New Comment"
			};

			reportingService.create(_.clone(newReport), params).then(() => {
			    reportingService.create(_.clone(newReport), params).then(() => {
				setTimeout(() => {
				    assert.fail('Should have failed');
				    done();
				});
			    }).catch(error => {
				setTimeout(() => {
				    assert.property(error, "code");
				    assert.equal(error.code, 401);
				    done();
				});
			    });
			}).catch(error => {
			    setTimeout(() => {
				assert.fail('Should have worked');
				done();
			    });
			});
		    });
		});

		it('should 401 on trying to report the stupidity with verdict', done => {
		    var newReport = {
			comment: "New Comment"
		    };
		    var params = {};
		    Stupidity.findOne().then(stupidity => {
			params = {
			    user: user,
			    stupidityId: stupidity.id
			};

			var report = new Report({stupidityId: stupidity.id});
			return report.save();
		    }).then(report => {
			report.isApproved = true;

			return report.save();
		    }).then(report => {
			assert.isOk(report.isApproved);
			
			reportingService.create(_.clone(newReport), params).then(() => {
			    setTimeout(() => {
				assert.fail('Should have failed');
				done();
			    });
			}).catch(error => {
			    setTimeout(() => {
				assert.property(error, "code");
				assert.equal(error.code, 401);
				done();
			    });
			});
		    }).catch(error => {
			debugger;
			setTimeout(() => {
			    assert.fail('Should have worked');
			    done();
			});
		    });
		});
	    });
	});
    });

    describe('voting', () => {
	it('should be able to vote for report', done => {
	    Stupidity.findOne().then(stupidity => {
		var params = {
		    user: users[1],
		    stupidityId: stupidity.id
		};
		var newReport = {
		    comment: "New Comment"
		};
		users[1].credibility.hits = 100;

		reportingService.create(_.clone(newReport), params).then(report => {
		    return reportingVotingService.create({
			vote: 'pro',
			comment: 'comment'
		    }, {
			user: user,
			stupidityId: stupidity.id
		    });
		}).then(() => {
		    return Report.findOne();
		}).then(onlyReport => {
		    setTimeout(() => {
			assert.notEqual(onlyReport.votesPro, 0, 'should have added a vote for deletion');
			done();
		    });
		}).catch(error => {
		    debugger;
		    setTimeout(() => {
			assert.fail('Should have worked');
			done();
		    });
		});
	    });
	});
    });

    describe('moderation', () => {
	it('should yield one random report', done => {
	    Stupidity.find({}).then(stupidities => {
		// all the reports should go live immediately
		user.credibility.hits = 100;

		var newReports = _.map(stupidities, stupidity => {
		    var params = {
			user: user,
			stupidityId: stupidity.id
		    };
		    var newReport = {
			comment: "New Comment"
		    };

		    return reportingService.create(newReport, params);
		});

		Promise.all(newReports).then(createdReports => {
		    return reportingService.find({user: users[1]});
		}).then(report => {
		    setTimeout(() => {
			assert.isArray(report, "is an object");
			assert.equal(report.length, 1, "should have one report");
			done();
		    });
		}).catch(error => {
		    setTimeout(() => {
			assert.fail('Should have worked');
			done();
		    });
		});
	    });
	});

	it('should auto-remove stupidity on treshold and generate congregated report', done => {
	    Stupidity.find({}).then(stupidities => {
		// all the reports should go live immediately
		user.credibility.hits = 100;

		// we report all the stupidities
		var newReports = _.map(stupidities, stupidity => {
		    var params = {
			user: user,
			stupidityId: stupidity.id
		    };
		    var newReport = {
			comment: "New Comment"
		    };

		    return reportingService.create(newReport, params);
		});

		Promise.all(newReports).then(createdReports => {
		    return stupidityService.find();
		}).then(stupidities => {
		    // all the reported stupidities should've been removed from the response
		    setTimeout(() => {
			assert.isArray(stupidities, "is an array");
			assert.equal(stupidities.length, 0, "should have no stupidities");
			done();
		    });
		}).catch(error => {
		    setTimeout(() => {
			assert.fail('Should have worked');
			done();
		    });
		});
	    });
	});

	it('reports consist of all the previously submitted reports', done => {
	    Stupidity.find({}).then(stupidities => {
		// all the reports should go live immediately
		users[0].credibility.hits = 5;
		users[1].credibility.hits = 6;

		var newReports = _.map(stupidities, stupidity => {
		    var params = [{
			user: users[0],
			stupidityId: stupidity.id
		    }, {
			user: users[1],
			stupidityId: stupidity.id
		    }];

		    var newReports = [{
			comment: "0"
		    }, {
			comment: "1"
		    }];

		    var reports = _.map(_.range(2), i => {
			return reportingService.create(newReports[i], params[i]);
		    });

		    return Promise.all(reports);
		});

		Promise.all(newReports).then(createdReports => {
		    return reportingService.find({user: users[2]});
		}).then(report => {
		    setTimeout(() => {
			assert.isArray(report, "is an object");
			assert.equal(report.length, 1, "should have one report");
			report = report[0];
			assert.equal(report.parts.length, 2, "should have two parts");
			// order is not guaranteed
			var has_0 = report.parts[0].comment == '0' || report.parts[1].comment == '0';
			var has_1 = report.parts[0].comment == '1' || report.parts[1].comment == '1';
			assert.isOk(has_0, "should have valid comment");
			assert.isOk(has_1, "should have valid comment");
			done();
		    });
		}).catch(error => {
		    setTimeout(() => {
			assert.fail('Should have worked');
			done();
		    });
		});
	    });
	});

	it('should yield reports only by other users', done => {
	    Stupidity.find({}).then(stupidities => {
		// all the reports should go live immediately
		user.credibility.hits = 100;

		var newReports = _.map(stupidities, stupidity => {
		    var params = {
			user: user,
			stupidityId: stupidity.id
		    };
		    var newReport = {
			comment: "New Comment"
		    };

		    return reportingService.create(newReport, params);
		});

		Promise.all(newReports).then(createdReports => {
		    return reportingService.find({user: user});
		}).then(report => {
		    setTimeout(() => {
			assert.isArray(report, "is an object");
			assert.equal(report.length, 0, "should have no reports");
			done();
		    });
		}).catch(error => {
		    setTimeout(() => {
			assert.fail('Should have worked');
			done();
		    });
		});
	    });
	});

	it('should yield [] when no reports are available', done => {
	    Report.count().then(count => {
		setTimeout(() => {
		    assert.equal(count, 0, "no reportes submitted");
		});

		return reportingService.find({user: user});
	    }).then(report => {
		setTimeout(() => {
		    assert.isArray(report, "is an array");
		    assert.equal(report.length, 0, "should be empty");
		    done();
		});
	    }).catch(error => {
		setTimeout(() => {
		    assert.fail('Should have worked');
		    done();
		});
	    });
	});

	it('should yield only un-voted reports', done => {
	    Stupidity.findOne().then(stupidity => {
		// all the reports should go live immediately
		users[0].credibility.hits = 100;

		var params = {
		    user: users[0],
		    stupidityId: stupidity.id
		};

		var newReport = {
		    comment: "New Comment"
		};

		reportingService.create(newReport, params).then(createdReports => {
		    return reportingVotingService.create({
			vote: 'pro',
			comment: 'comment'
		    }, {
			user: users[1],
			stupidityId: stupidity.id
		    });
		}).then(() => {
		    return reportingService.find({user: users[1]});
		}).then(report => {
		    setTimeout(() => {
			assert.equal(report.length, 0, "should have no reports");
			done();
		    });
		}).catch(error => {
		    setTimeout(() => {
			assert.fail('Should have worked');
			done();
		    });
		});
	    });
	});

	it('should return the stupidity on rejected report', done => {
	    Stupidity.find({}).then(stupidities => {
		// all the reports should go live immediately
		user.credibility.hits = 11;

		// we report all the stupidities
		var newReports = _.map(stupidities, stupidity => {
		    var params = {
			user: user,
			stupidityId: stupidity.id
		    };
		    var newReport = {
			comment: "New Comment"
		    };

		    return reportingService.create(newReport, params);
		});
		var stupidityToSave = stupidities[0];

		Promise.all(newReports).then(createdReports => {
		    return stupidityService.find();
		}).then(stupidities => {
		    // all the reported stupidities should've been removed from the response
		    setTimeout(() => {
			assert.isArray(stupidities, "is an array");
			assert.equal(stupidities.length, 0, "should have no stupidities");
		    });

		    users[1].credibility.hits = 100;
		    return reportingVotingService.create({
			vote: 'contra',
			comment: 'comment'
		    }, {
			user: users[1],
			stupidityId: stupidityToSave.id
		    });
		}).then(() => {
		    return stupidityService.find();
		}).then(stupidities => {
		    // all the reported stupidities should've been removed from the response
		    setTimeout(() => {
			assert.isArray(stupidities, "is an array");
			assert.equal(stupidities.length, 1, "should have a stupidity back");
			assert.equal(stupidities[0].id, stupidityToSave.id, "should have a stupidity back");
			done();
		    });
		}).catch(error => {
		    setTimeout(() => {
			assert.fail('Should have worked');
			done();
		    });
		});
	    });
	});

	it('should auto-verdict on vote threshold', done => {
	    Stupidity.findOne().then(stupidity => {
		var params = {
		    user: users[1],
		    stupidityId: stupidity.id
		};
		var newReport = {
		    comment: "New Comment"
		};
		users[1].credibility.hits = 100;

		reportingService.create(_.clone(newReport), params).then(report => {
		    users[0].credibility.hits = 100;
		    return reportingVotingService.create({
			vote: 'pro',
			comment: 'comment'
		    }, {
			user: users[0],
			stupidityId: stupidity.id
		    });
		}).then(() => {
		    return Report.findOne();
		}).then(onlyReport => {
		    setTimeout(() => {
			assert.notEqual(onlyReport.voteCount, 0, 'should have added a vote');
			assert.isOk(onlyReport.hasVerdict, 'should have a verdict');
			assert.isOk(onlyReport.isApproved, 'should be approved');
			done();
		    });
		}).catch(error => {
		    setTimeout(() => {
			assert.fail('Should have worked');
			done();
		    });
		});
	    });
	});
    });

    describe('user', () => {
	describe('votes', () => {
	    it('should be given for the accepted report', done => {
		Stupidity.findOne().then(stupidity => {
		    var params = {
			user: users[1],
			stupidityId: stupidity.id
		    };
		    var newReport = {
			comment: "New Comment"
		    };
		    users[1].credibility.hits = 100;
		    assert.equal(users[1].votes.bought, 0, 'should have no votes');

		    reportingService.create(_.clone(newReport), params).then(report => {
			users[0].credibility.hits = 100;
			return reportingVotingService.create({
			    vote: 'pro',
			    comment: 'comment'
			}, {
			    user: users[0],
			    stupidityId: stupidity.id
			});
		    }).then(() => {
			return User.findOne({_id: users[1].id});
		    }).then(user => {
			setTimeout(() => {
			    assert.notEqual(user.votes.bought, 0, 'should have added a vote');
			    done();
			});
		    }).catch(error => {
			debugger;
			setTimeout(() => {
			    assert.fail('Should have worked');
			    done();
			});
		    });
		});
	    });

	    it('should be given for complementary verdict on report', done => {
		Stupidity.findOne().then(stupidity => {
		    var creator = users[1];
		    creator.credibility.hits = 10;
		    var params = {
			user: creator,
			stupidityId: stupidity.id
		    };
		    var newReport = {
			comment: "New Comment"
		    };
		    var voter = users[2];
		    var lasthitter = users[3];
		    lasthitter.credibility.hits = 100;

		    var createdReport = {};
		    reportingService.create(_.clone(newReport), params).then(report => {
			createdReport = report;
			return reportingVotingService.create({
			    vote: 'pro',
			    comment: 'comment'
			}, {
			    user: voter,
			    comment: "comment",
			    stupidityId: stupidity.id
			});
		    }).then(() => {
			return Report.findOne({stupidityId: stupidity.id});
		    }).then(report => {
			setTimeout(() => {
			    assert.isNotOk(report.hasVerdict, "shouldn't have a verdict yet");
			});

			return reportingVotingService.create({
			    vote: 'pro',
			    comment: 'comment'
			}, {
			    user: lasthitter,
			    comment: "comment",
			    stupidityId: stupidity.id
			});
		    }).then(() => {
			return Report.findOne({stupidityId: stupidity.id});
		    }).then(report => {
			setTimeout(() => {
			    assert.isOk(report.hasVerdict, "should have a verdict");
			});

			return User.findOne({_id: voter._id});
		    }).then(user => {
			setTimeout(() => {
			    assert.notEqual(user.votes.bought, 0, "should have been awarded");
			});

			return User.findOne({_id: lasthitter._id});
		    }).then(user => {
			setTimeout(() => {
			    assert.notEqual(user.votes.bought, 0, "should have been awarded");
			    done();
			});
		    }).catch(error => {
			setTimeout(() => {
			    assert.fail('Should have worked');
			    done();
			});
		    });
		});
	    });
	});

	describe('credibility', () => {
	    it('should increase on complementary verdict', done => {
		Stupidity.findOne().then(stupidity => {
		    var creator = users[1];
		    creator.credibility.hits = 10;
		    var params = {
			user: creator,
			stupidityId: stupidity.id
		    };
		    var newReport = {
			comment: "New Comment"
		    };
		    var voter = users[2];
		    var lasthitter = users[3];
		    lasthitter.credibility.hits = 100;

		    var createdReport = {};
		    reportingService.create(_.clone(newReport), params).then(report => {
			createdReport = report;
			return reportingVotingService.create({
			    vote: 'pro',
			    comment: 'comment'
			}, {
			    user: voter,
			    comment: "comment",
			    stupidityId: stupidity.id
			});
		    }).then(() => {
			return Report.findOne({stupidityId: stupidity.id});
		    }).then(report => {
			setTimeout(() => {
			    assert.isNotOk(report.hasVerdict, "shouldn't have a verdict yet");
			});

			return reportingVotingService.create({
			    vote: 'pro',
			    comment: 'comment'
			}, {
			    user: lasthitter,
			    comment: "comment",
			    stupidityId: stupidity.id
			});
		    }).then(() => {
			return Report.findOne({stupidityId: stupidity.id});
		    }).then(report => {
			setTimeout(() => {
			    assert.isOk(report.hasVerdict, "should have a verdict");
			});

			return User.findOne({_id: voter._id});
		    }).then(user => {
			setTimeout(() => {
			    assert.notEqual(user.credibility.hits, 0, "should have been awarded");
			});

			return User.findOne({_id: lasthitter._id});
		    }).then(user => {
			setTimeout(() => {
			    assert.notEqual(user.credibility.hits, 0, "should have been awarded");
			    done();
			});
		    }).catch(error => {
			setTimeout(() => {
			    assert.fail('Should have worked');
			    done();
			});
		    });
		});
	    });

	    it('should decrease on non-complementary verdict', done => {
		Stupidity.findOne().then(stupidity => {
		    var creator = users[1];
		    creator.credibility.hits = 10;
		    var params = {
			user: creator,
			stupidityId: stupidity.id
		    };
		    var newReport = {
			comment: "New Comment"
		    };
		    var voter = users[2];
		    var lasthitter = users[3];
		    lasthitter.credibility.hits = 100;

		    var createdReport = {};
		    reportingService.create(_.clone(newReport), params).then(report => {
			createdReport = report;
			return reportingVotingService.create({
			    vote: 'contra',
			    comment: 'comment'
			}, {
			    user: voter,
			    comment: "comment",
			    stupidityId: stupidity.id
			});
		    }).then(() => {
			return Report.findOne({stupidityId: stupidity.id});
		    }).then(report => {
			setTimeout(() => {
			    assert.isNotOk(report.hasVerdict, "shouldn't have a verdict yet");
			});

			return reportingVotingService.create({
			    vote: 'pro',
			    comment: 'comment'
			}, {
			    user: lasthitter,
			    comment: "comment",
			    stupidityId: stupidity.id
			});
		    }).then(() => {
			return Report.findOne({stupidityId: stupidity.id});
		    }).then(report => {
			setTimeout(() => {
			    assert.isOk(report.hasVerdict, "should have a verdict");
			});

			return User.findOne({_id: voter._id});
		    }).then(user => {
			setTimeout(() => {
			    assert.notEqual(user.credibility.misses, 0, "should have been punished");
			});

			return User.findOne({_id: lasthitter._id});
		    }).then(user => {
			setTimeout(() => {
			    assert.notEqual(user.credibility.hits, 0, "should have been awarded");
			    done();
			});
		    }).catch(error => {
			setTimeout(() => {
			    assert.fail('Should have worked');
			    done();
			});
		    });
		});
	    });
	});
    });
});
