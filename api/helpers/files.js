var crypto = require('crypto');
var fs = require('fs');

function decodeBase64Image(dataString) {
    var matches = dataString.match(/^data:image\/jpe{0,1}g;base64,(.+)$/);

    if(!matches || matches.length !== 2) {
        throw "Invalid image format";
    }

    return new Buffer(matches[1], 'base64');
}

module.exports = {
    saveImageFromBase64: function(dataString) {
        return new Promise((resolve, reject) => {
	    var data = decodeBase64Image(dataString);

            var sha = crypto.createHash('sha1').update(data).digest('hex');
            var path = "./public/images/" + sha;

            fs.writeFile(path, data, function(error) {
                if(error) {
                    reject(error);
                } else {
                    resolve(sha);
                }
            });
        });
    }
};
