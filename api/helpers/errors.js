module.exports = {
    400: {
        code: 400,
        message: 'Invalid data'
    },
    401: {
	code: 401,
	message: 'Get out of here, stalker'
    },
    403: {
        code: 403,
        message: 'Get out of here, stalker'
    },
    404: {
        code: 404,
        message: 'Not found'
    },
    451: {
        code: 451,
        message: 'Censored'
    },
    500: {
        code: 500,
        message: 'Internal server error'
    }
}
