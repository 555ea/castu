var mongoose = require('mongoose');

module.exports = {
    location: function(location) {
        // if location has a radius - it should be greater than 10 units
        if(location.radius) {
            if(location.radius < 10) {
                return false;
            }
        }

        // locations should always have a latitude and longitude, within bounds
        if(    location.longitude
            && location.latitude
            && location.longitude < 360
            && location.longitude > -360
            && location.latitude < 360
            && location.latitude  > -360 ) {
            return true;
        } else {
            return false;
        }
    },
    objectId: function(id) {
        id = String(id);
        return (id.length == 24) && mongoose.Types.ObjectId.isValid(id);
    }
}
