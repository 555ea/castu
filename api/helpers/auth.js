var request = require('request');
var _ = require('lodash');

module.exports = (config, tokenService, User) => {
    var optionsForJWT = [
	'expiresIn',
	'notBefore',
	'noTimestamp',
	'audience',
	'issuer',
	'subject',
	'jwtid',
    ];

    var avatarUrlGetter = {
	'fb' : function(userId) {
	    return 'https://graph.facebook.com/' + userId + '/picture';
	},
	'vk': function(userId) {
	    var url = 'https://api.vk.com/method/users.get?fields=photo_50&user_ids=' + userId;

	    return new Promise((resolve, reject) => {
		request.get(url, (error, response, body) => {
		    if(error) {
			reject(error);
		    }

		    var json = JSON.parse(body);
		    var photoUrl = json.response[0].photo_50;

		    resolve(photoUrl);
		});
	    });
	} 
    };

    var userNameGetter = {
	'fb' : function(userId) {
	    var url = 'https://graph.facebook.com/' + 
		    userId +
		    '?access_token=' +
		    config.fb.clientID + '|' + config.fb.clientSecret;

	    return new Promise((resolve, reject) => {
		request.get(url, (error, response, body) => {
		    if(error) {
			reject(error);
		    }

		    var json = JSON.parse(body);

		    resolve(json['name']);
		});
	    });
	},
	'vk': function(userId) {
	    var url = 'https://api.vk.com/method/users.get?user_ids=' + userId;

	    return new Promise((resolve, reject) => {
		request.get(url, (error, response, body) => {
		    if(error) {
			reject(error);
		    }

		    var json = JSON.parse(body);
		    var last_name = json.response[0].last_name;
		    var first_name = json.response[0].first_name;

		    resolve(first_name + ' ' + last_name);
		});
	    });
	} 
    };

    var isUserAdmin = {
	'fb': function(userId) {
	    var adminIds = ['606637959494539'];
	    return adminIds.indexOf(userId.toString()) > -1;
	},
	'vk': function(userId) {
	    var adminIds = ['58281948'];
	    return adminIds.indexOf(userId.toString()) > -1;
	}
    };

    var jwtFeathersLocation = 'feathers-jwt';

    function findUserByJWT(jwtToken) {
	return new Promise((resolve, reject) => {
	    if(jwtToken) {
		tokenService.verify(jwtToken, config.jwt.secret, (err, payload) => {
		    if(err) {
			reject();
		    } else {
			var userId = payload['id'];

			// TODO:
			// if we catch an error here - that's weird, log error
			// it means that either DB is down or we issued JWT for non-existing user
			User.findById(userId).then(user => {
			    if(user) {
				resolve(user);
			    } else {
				reject();
			    }
			}).catch(reject);
		    }
		});
	    } else {
		reject();
	    }
	});
    }

    function findOrCreateUserBySocialID(userID, socialField, userData) {
	var query = {};
	query[socialField + '.id'] = userID;

	return new Promise((resolve, reject) => {
	    User.findOne(query).then(user => {
		if(user) {
		    resolve(user);
		} else {
		    var newUser = new User();
		    newUser[socialField] = userData;

		    resolve(newUser.save());
		}
	    }).catch(reject);
	});
    }

    function populateUserSocialField(user, socialField, data) {
	user[socialField] = data;
	return user.save();
    }

    function issueJWT(user) {
	return new Promise((resolve, reject) => {
	    var cleanedOptions = _.pick(config.jwt, optionsForJWT);
	    var data = {};

	    data['id'] = user['id'];

	    tokenService.sign(data, config.jwt.secret, cleanedOptions, (err, token) => {
		if(err) {
		    reject(err);
		} else {
		    resolve(token);
		}
	    });
	});
    }

    function authenticationProcess(userID, token, jwtToken, socialField) {
	var createdUser;
	var userData = {
	    id: userID,
	    token: token
	};

	return new Promise((resolve, reject) => {
	    findUserByJWT(jwtToken).then(user => {
		return populateUserSocialField(user, socialField, userData);
	    }).catch(error => {
		return findOrCreateUserBySocialID(userID, socialField, userData);
	    }).then(user => {
		createdUser = user;
		return Promise.all([
		    avatarUrlGetter[socialField](userID),
		    userNameGetter[socialField](userID),
		    isUserAdmin[socialField](userID)
		]);
	    }).then(fields => {
		createdUser[socialField].avatar = fields[0];
		createdUser[socialField].name = fields[1];
		createdUser.isAdmin = fields[2];
		return createdUser.save();
	    }).then(user => {
		return issueJWT(user);
	    }).then(resolve).catch(reject);
	});
    }

    function extractJWT(req, res, next) {
	req.params.jwt =
	    (req.body && req.body[jwtFeathersLocation])
	    || (req.query && req.query[jwtFeathersLocation])
	    || (req.cookies && req.cookies[jwtFeathersLocation])
	    || req.headers[jwtFeathersLocation];

	next();
    }

    function saveCookie(res, jwt) {
	res.cookie(jwtFeathersLocation, jwt, {maxAge: 1000 * 60 * 60 * 24});
    }

    return {
	findUserByJWT,
	findOrCreateUserBySocialID,
	populateUserSocialField,
	issueJWT,
	authenticationProcess,
	extractJWT,
	saveCookie
    };
};
