swagger: "2.0"
info:
    version: "0.0.1"
    title: Catastrophic Stupidity
host: localhost:10010
basePath: /rest/
schemes:
    - http
    - https
consumes:
    - application/json
produces:
    - application/json
paths:
    /cities:
        get:
            description: Get available cities
            responses:
                "200":
                    description: Success
                    schema:
                        $ref: "#/definitions/Cities"

    # done
    /stupidity/{id}:
        get:
            description: Get the stupidity by its ID
            parameters:
                - name: id
                  in: path
                  required: true
                  type: string
            responses:
                "200":
                    description: Success
                    schema:
                        $ref: "#/definitions/StupidityDescription"
                "404":
                    description: Stupidity not found
                "451":
                    description: Stupidity was censored by pidrahuys

    /stupidity/{id}/fix:
        post:
            description: Send photo of the fixed stupidity
            parameters:
                - name: id
                  description: Stupidity ID
                  in: path
                  required: true
                  type: string
                - name: fix
                  description: Stupidity fix
                  in: body
                  schema:
                      $ref: '#/definitions/StupidityFixSubmitted'
            responses:
                "200":
                    description: Success
                "404":
                    description: Stupidity not found
                "451":
                    description: Stupidity was censored by pidrahuys
                "401":
                    description: You have to be logged in
                "403":
                    description: You are banned

    /stupidity/{id}/report:
        post:
            description: Report stupidity for rule violation
            parameters:
                - name: id
                  description: Stupidity ID
                  in: path
                  required: true
                  type: string
                - name: comment
                  description: Comment to the report
                  in: formData
                  type: string
            responses:
                "200":
                    description: Success
                    schema:
                        type: string
                        description: ID of the added report
                "404":
                    description: Stupidity not found
                "451":
                    description: Stupidity was censored by pidrahuys
                "401":
                    description: You have to be logged in
                "403":
                    description: You are banned

    /stupidity/{id}/vote:
        get:
            description: Vote for the stupidity
            parameters:
                - name: id
                  in: path
                  required: true
                  type: string
                - name: count
                  description: Number of votes to give
                  in: query
                  required: true
                  type: integer
                  minimum: 1
            responses:
                "200":
                    description: Success
                "404":
                    description: Stupidity not found
                "451":
                    description: Stupidity was censored by pidrahuys
                "401":
                    description: You have to be logged in
                "403":
                    description: You have no votes left

    /stupidity:
        post:
            description: Add a stupidity
            parameters:
                - name: stupidity
                  description: Stupidity descriptor
                  in: body
                  schema:
                      $ref: '#/definitions/StupiditySubmitted'
            responses:
                "200":
                    description: Success
                    schema:
                        properties:
                          id:
                            type: string
                          image:
                            type: string
                "401":
                    description: You have to be logged in
                "403":
                    description: You are banned

    /stupidities:
        get:
            description: Get stupidities in the given location
            parameters:
                - name: location
                  description: Location to search in
                  in: body
                  schema:
                      $ref: '#/definitions/Location'
            responses:
                "200":
                    description: Success
                    schema:
                        $ref: '#/definitions/StupidityDescriptions'

    /report/{id}:
        get:
            description: Get the report by its ID
            parameters:
                - name: id
                  in: path
                  required: true
                  type: string
            responses:
                "200":
                    description: Success
                    schema:
                        $ref: "#/definitions/Report"
                "404":
                    description: Report not found

    /report/{id}/votePro:
        get:
            description: Vote for retention of reported stupidity
            parameters:
                - name: id
                  in: path
                  required: true
                  type: string
            responses:
                "200":
                    description: Success
                "404":
                    description: Report not found
                "401":
                    description: You have to be logged in
                "403":
                    description: You have already voted

    /report/{id}/voteContra:
        get:
            description: Vote for deletion of reported stupidity
            parameters:
                - name: id
                  in: path
                  required: true
                  type: string
            responses:
                "200":
                    description: Success
                "404":
                    description: Report not found
                "401":
                    description: You have to be logged in
                "403":
                    description: You have already voted

    /report/random:
        get:
            description: Get random report to participate in moderation
            responses:
                "200":
                    description: Success
                    schema:
                        $ref: "#/definitions/Report"
                "404":
                    description: No reports to moderate

    /swagger:
        x-swagger-pipe: swagger_raw

definitions:
    Location:
        required:
            - latitude
            - longitude
        properties:
            latitude:
                type: number
                minimum: -360
                maximum: 360
            longitude:
                type: number
                minimum: -360
                maximum: 360
            radius:
                type: number
                minimum: 10

    City:
        required:
            - id
            - name
            - location
        properties:
            id:
                description: City ID
                type: string
            name:
                description: City name
                type: string
            location:
                description: Geometrical center of the city, with its radius
                $ref: '#/definitions/Location'

    Cities:
        type: array
        items:
            $ref: '#/definitions/City'

    StupiditySubmitted:
        required:
            - location
            - image
            - comment
        properties:
            location:
                description: Location of the stupidity
                $ref: '#/definitions/Location'
            image:
                description: Photo of the stupidity
                type: string
                format: binary
            comment:
                description: Submitter comment
                type: string
            level:
                description: Stupidity level
                type: string
            categories:
                description: Category array
                type: array
                items:
                  type: string

    StupidityDescription:
        required:
            - id
            - location
            - timestamp
            - image
            - votes
        properties:
            id:
                description: Stupidity ID
                type: string
            location:
                description: Location of the stupidity
                $ref: '#/definitions/Location'
            timestamp:
                description: Stupidity upload time, UNIX format
                type: number
            image:
                description: Hash of the stupidity photo
                type: string
            votes:
                description: Vote count for this stupidity
                type: integer
            comment:
                description: Submitter comment
                type: string
            submitter:
                description: User ID of the submitter (ADMIN ONLY)
                type: string
            fix:
                description: Fix of this stupidity, if available
                $ref: '#/definitions/StupidityFixDescription'

    StupidityDescriptions:
        type: array
        items:
            $ref: '#/definitions/StupidityDescription'

    Report:
        required:
            - stupidityId
            - comment
        properties:
            stupidityId:
                description: ID of the reported stupidity
                type: string
            comment:
                description: Cause of the report
                type: string
            submitter:
                description: User ID of the submitter (ADMIN ONLY)
                type: string
            pro:
                description: Votes against removal (ADMIN ONLY)
                type: integer
            contra:
                description: Votes for removal (ADMIN ONLY)
                type: integer

    StupidityFixSubmitted:
        required:
            - image
            - comment
        properties:
            image:
                description: Photo of the stupidity fix
                type: string
                format: binary
            comment:
                description: Submitter comment
                type: string

    StupidityFixDescription:
        required:
            - timestamp
            - image
            - votes
        properties:
            timestamp:
                description: Fix upload time, UNIX format
                type: number
            image:
                description: Hash of the fix photo
                type: string
            votes:
                description: Percentage of people who consider this fix proper
                type: number
            comment:
                description: Submitter comment
                type: string
            isAccepted:
                description: Is this fix accepted?
                type: boolean
