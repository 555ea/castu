var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    // 0 - "not a solution, violates rules, remove"
    // 1 - "very bad solution, redo plz"
    // 10 - "problem solved"
    acceptance: {type: Number, min: 0, max: 10},
    comment: {type: String},
    submitterId: {type: Schema.ObjectId, ref: 'Users', required: true},
    stupidityFixId: {type: Schema.ObjectId, ref: 'Fixes', required: true}
}, {
    timestamps: true   
});

schema.statics.create = function(fix, user, acceptance, comment) {
    var newVote = new this({
	acceptance: acceptance,
	comment: comment,
	submitterId: user.id,
	stupidityFixId: fix.id
    });

    var promises = [
	fix.vote(user.credibility.value, acceptance),
	newVote.save()
    ];

    return Promise.all(promises);
};

var Fixes = mongoose.model('FixesVotes', schema);

module.exports = Fixes;
