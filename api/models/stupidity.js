var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    location: {
        type: [Number], // [longitude, latitude] (order is important)
        index: '2dsphere',
	required: true
    },
    image: {type: String, required: true},
    voteCount: {type: Number, default: 0},
    comment: {type: String},
    isHidden: {type: Boolean, default: false},
    severity: {type: Number, min: 0, max: 2, required: true},
    category: {type: Number, min: 0, max: 8, required: true},
    submitterId: {type: Schema.ObjectId, ref: 'Users', required: true}
}, {
    timestamps: true   
});

schema.pre('save', function(next) {
    // enforce 0 votes on creation
    if(this.isNew) {
        this.voteCount = schema.paths.voteCount.default();
	this.isHidden = schema.paths.isHidden.default();
    }
    next();
});

schema.methods.vote = function(voteCount) {
    this.voteCount += voteCount;
    return this.save();
};

var Stupidity = mongoose.model('Stupidities', schema);

module.exports = Stupidity;
