var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var _ = require('lodash');

const SETTINGS = {
    report_treshold: 1,
    removal_treshold: 20,
    saving_treshold: 10,
    punishments: {
	credibility: {
	    accepted: 1,
	    rejected: 1
	}
    },
    awards: {
	votes: {
	    accepted: 3,
	    rejected: 3
	},
	credibility: {
	    accepted: 1,
	    rejected: 1
	}
    }
};

var schema = new Schema({
    votesPro: {type: Number, default: 0},
    votesContra: {type: Number, default: 0},
    isApproved: {type: Boolean, default: false},
    isRejected: {type: Boolean, default: false},
    stupidityId: {type: Schema.ObjectId, ref: 'Stupidities', required: true}
}, {
    timestamps: true   
});

schema.virtual('voteCount').get(function() {
    return this.votesPro + this.votesContra;
});

schema.virtual('hasVerdict').get(function() {
    return this.isApproved || this.isRejected;
});

schema.statics.addReportPart = function(reportPart, credibility) {
    var Report = this;

    return new Promise((resolve, reject) => {
	Report.findOne({
	    stupidityId: reportPart.stupidityId
	}).then(report => {
	    if(!report) {
		var newReport = new Report({
		    stupidityId: reportPart.stupidityId
		});

		return newReport.save();
	    } else {
		return report;
	    }
	}).then(report => {
	    const votingForRemoval = reportPart.isPro;
	    return report.vote(credibility, votingForRemoval);
	}).then(() => {
	    resolve(reportPart);
	}).catch(reject);
    });
};

schema.methods.vote = function(credibility, isPro) {
    var reportWasNotActive = this.votesPro < SETTINGS.report_treshold;

    if(isPro) {
	this.votesPro += credibility;
    } else {
	this.votesContra += credibility;
    }

    // even if user could one-shot the report - we still need one more vote
    if(reportWasNotActive && this.votesPro >= SETTINGS.report_treshold) {
	return this.activateReport();
    } else if(this.votesPro >= SETTINGS.removal_treshold) {
    debugger;
	return this.accept();
    } else if(this.votesContra >= SETTINGS.saving_treshold) {
    debugger;
	return this.reject();
    } else {
    debugger;
	return this.save();
    }
};

schema.methods.activateReport = function() {
    return new Promise((resolve, reject) => {
	mongoose.model('Stupidities').findById(this.stupidityId).then(stupidity => {
	    // new reports won't be received, stupidity won't be shown in the timeline
	    stupidity.isHidden = true;
	    return Promise.all([stupidity.save(), this.save()]);
	}).then(resolve).catch(reject);
    });
};

schema.methods.getReportParts = function() {
    var ReportPart = mongoose.model('ReportsParts');

    return ReportPart.find({
	stupidityId: this.stupidityId
    }).populate('submitterId');
};

schema.methods.accept = function() {
    return new Promise((resolve, reject) => {
	this.isApproved = true;

	this.getReportParts().then(reportParts => {
	    return Promise.all(_.map(reportParts, reportPart => {
		// punish trolls, award conformists
		if(reportPart.isPro) {
		    return reportPart.submitterId.credibilityHit(
			SETTINGS.awards.votes.accepted,
			SETTINGS.awards.credibility.accepted
		    );
		} else {
		    return reportPart.submitterId.credibilityMiss(
			SETTINGS.punishments.credibility.accepted
		    );
		}
	    }));
	}).then(() => {
	    return this.save();
	}).then(resolve).catch(err => {
	    reject(err);
	});
    });
};

schema.methods.reject = function() {
    return new Promise((resolve, reject) => {
	this.isRejected = true;

	this.getReportParts().then(reportParts => {
	    return Promise.all(_.map(reportParts, reportPart => {
		// punish trolls, award conformists
		if(!reportPart.isPro) {
		    return reportPart.submitterId.credibilityHit(
			SETTINGS.awards.votes.rejected,
			SETTINGS.awards.credibility.rejected
		    );
		} else {
		    return reportPart.submitterId.credibilityMiss(
			SETTINGS.punishments.credibility.rejected
		    );
		}
	    }));
	}).then(() => {
	    return mongoose.model('Stupidities').findById(this.stupidityId);
	}).then(stupidity => {
	    // return stupidity to the timeline
	    stupidity.isHidden = false;
	    return Promise.all([stupidity.save(), this.save()]);
	}).then(resolve).catch(err => {
	    reject(err);
	});
    });
};

var Report = mongoose.model('Reports', schema);

module.exports = Report;
