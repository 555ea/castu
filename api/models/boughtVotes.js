var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    userId: {type: Schema.ObjectId, ref: 'Users'},
    timestamp: {type: Date, default: Date.now},
    voteCount: {type: Number, required: true}
});

var BoughtVotes = mongoose.model('BoughtVotes', schema);

module.exports = BoughtVotes;
