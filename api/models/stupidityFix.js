var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var _ = require('lodash');

const SETTINGS = {
    vote_treshold: 10,
    punishments: {
        credibility: {
            rejected: 3
        }
    },
    awards: {
        votes: {
            accepted: {
                max: 10,
                min: 3
            },
            rejected: 5
        },
        credibility: {
            accepted: 3,
            rejected: 1
        }
    }
};

var schema = new Schema({
    image: {type: String, required: true},
    voteCount: {type: Number, default: 0},
    isApproved: {type: Boolean, default: false},
    isRejected: {type: Boolean, default: false},
    comment: {type: String},
    submitterId: {type: Schema.ObjectId, ref: 'Users', required: true},
    stupidityId: {type: Schema.ObjectId, ref: 'Stupidities', required: true}
}, {
    timestamps: true   
});

schema.pre('save', function(next) {
    // enforce 0 votes on creation
    if(this.isNew) {
        this.voteCount = schema.paths.voteCount.default();
        this.isApproved = schema.paths.isApproved.default();
        this.isRejected = schema.paths.isApproved.default();
    }
    next();
});

schema.virtual('hasVerdict').get(function() {
    return this.isApproved || this.isRejected;
});

schema.methods.vote = function(credibility, acceptance) {
    this.voteCount += credibility * (acceptance == 0 ? -1 : 1);

    if(this.voteCount >= SETTINGS.vote_treshold) {
        return this.accept();
    } else if(this.voteCount <= -SETTINGS.vote_treshold) {
        return this.reject();
    } else {
        return this.save();
    }
};

schema.methods.getVotes = function() {
    var FixVote = mongoose.model('FixesVotes');

    return FixVote.find({
        stupidityFixId: this._id
    }).populate('submitterId');
};

function awardsForVotes(votes) {
    var acceptances = _.map(votes, vote => {
        return vote.acceptance;
    });
    var positiveVotes = _.filter(acceptances, vote => {
        return vote > 0;
    });

    // the more users voted the same - the bigger is user award
    var voteAwards = _.countBy(positiveVotes);

    _.forEach(voteAwards, (acceptance, count) => {
        // recalculate vote award based on occurance frequency
        var frequency = count / positiveVotes.length;
        // but always give a reasonable amount of votes
        voteAwards[acceptance] = Math.max(
            Math.floor(SETTINGS.awards.votes.accepted.max * frequency),
            SETTINGS.awards.votes.accepted.min
        );
    });

    return voteAwards;
}

schema.methods.accept = function() {
    return new Promise((resolve, reject) => {
        this.isApproved = true;

        mongoose.model('Users').findOne({_id: this.submitterId}).then(user => {
            var userModel = mongoose.model('Users');

            return user.credibilityHit(
                SETTINGS.awards.votes.accepted.max,
                SETTINGS.awards.credibility.accepted
            );
        }).then(user => {
            return this.getVotes();
        }).then(votes => {
            var voteAwards = awardsForVotes(votes);

            return Promise.all(_.map(votes, vote => {
                // punish trolls, award users based on their conformity
                if(vote.acceptance <= 0) {
                    return vote.submitterId.credibilityMiss();
                } else {
                    return vote.submitterId.credibilityHit(voteAwards[vote.acceptance]);
                }
            }));
        }).then(() => {
            return this.save();
        }).then(resolve).catch(reject);
    });
};

schema.methods.reject = function() {
    return new Promise((resolve, reject) => {
	this.isRejected = true;

	mongoose.model('Users').findOne({_id: this.submitterId}).then(user => {
	    return user.credibilityMiss(SETTINGS.punishments.credibility.rejected);
	}).then(user => {
	    return this.getVotes();
	}).then(votes => {
	    return Promise.all(_.map(votes, vote => {
		// community decided that it was a faux fix, punish accepters
		if(vote.acceptance > 0) {
		    return vote.submitterId.credibilityMiss();
		} else {
		    return vote.submitterId.credibilityHit(SETTINGS.awards.votes.rejected);
		}
	    }));
	}).then(() => {
	    return this.save();
	}).then(resolve).catch(err => {
	    reject(err);
	});
    });
};

var Fix = mongoose.model('Fixes', schema);

module.exports = Fix;
