var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    stupidityId: {type: Schema.ObjectId, ref: 'Stupidities'},
    userId: {type: Schema.ObjectId, ref: 'Users'},
    voteCount: {type: Number, required: true}
}, {
    timestamps: true   
});

schema.statics.createOrUpdate = function(stupidity, user, voteCount) {
    var ids = {
	stupidityId: stupidity.id,
	userId: user.id
    };

    return new Promise((resolve, reject) => {
	this.findOne(ids).then(vote => {
	    return new Promise((resolve, reject) => {
		if(vote) {
		    // user has voted for this stupidity before
		    if(voteCount == 1 && user.votesLeft <= 0) {
			// unable to re-vote for free for the same stupidity
			reject();
		    } else {
			user.vote(voteCount).then(votesLeft => {
			    // can add more additional votes
			    vote.voteCount += voteCount;

			    resolve(vote);
			}).catch(reject);
		    }
		} else {
		    // didn't vote for this stupidity yet
		    var effectiveVoteCount = (voteCount == 1) ? 0 : voteCount;

		    user.vote(effectiveVoteCount).then(votesLeft => {
			var newVote = new this({
			    stupidityId: ids.stupidityId,
			    userId: ids.userId,
			    voteCount: voteCount
			});

			resolve(newVote);
		    }).catch(reject);
		}
	    });
	}).then(voteToSave => {
	    return Promise.all([
		stupidity.vote(voteCount),
		voteToSave.save()
	    ]);
	}).then(result => {
	    // return resulting vote count
	    resolve(result[0].voteCount);
	}).catch(reject);
    });
}

var StupidityVote = mongoose.model('StupidityVotes', schema);

module.exports = StupidityVote;
