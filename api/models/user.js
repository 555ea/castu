var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    lastLogin: {type: Date, default: Date.now},
    isBanned: {type: Boolean, default: false},
    isAdmin: {type: Boolean, default: false},
    votes: {
        free: {type: Number, default: 10, min: 0},
        freeCap: {type: Number, default: 10},
        bought: {type: Number, default: 0, min: 0}
    },
    credibility: {
        hits: {type: Number, default: 0},
        misses: {type: Number, default: 0}
    },
    vk: {
        id: {type: String},
        token: {type: String},
        name: {type: String},
	avatar: {type: String}
    },
    fb: {
        id: {type: String},
        token: {type: String},
        name: {type: String},
	avatar: {type: String}
    }
});

schema.virtual('votesLeft').get(function() {
    return this.votes.free + this.votes.bought;
});

schema.virtual('credibility.value').get(function() {
    const MINIMAL_CREDIBILITY = 1;

    var value = this.credibility.hits - this.credibility.misses;

    if(value <= MINIMAL_CREDIBILITY) {
        value = MINIMAL_CREDIBILITY;
    }

    return value;
});

schema.methods.vote = function(voteCount) {
    return new Promise((resolve, reject) => {
        if(this.isBanned) {
            reject();
        } else if(voteCount == 0) {
            resolve(this.votesLeft);
        } else if(voteCount <= this.votesLeft) {
            if(this.votes.free < voteCount) {
                // if we got here - we have bought votes
                this.votes.bought -= (voteCount - this.votes.free);
                this.votes.free = 0;
            } else {
                this.votes.free -= voteCount;
            }

            return this.save()
                .then(user => {resolve(user.votesLeft);})
                .catch(reject);
        } else {
            // not enough votes
            reject();
        }
    });
};

schema.methods.resetFreeVotes = function() {
    this.votes.free = this.votes.freeCap;
};

schema.methods.addVotes = function(voteCount) {
    this.votes.bought += voteCount;
};

schema.methods.credibilityHit = function(voteAward, severity) {
    if(!severity) {
	severity = 1;
    }
    
    this.credibility.hits += severity;

    if(voteAward) {
        this.addVotes(voteAward);
    }

    return this.save();
};

schema.methods.credibilityMiss = function(severity) {
    if(!severity) {
	severity = 1;
    }
    
    this.credibility.misses += severity;

    return this.save();
};

var User = mongoose.model('Users', schema);

module.exports = User;
