var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    adminId: {type: Schema.ObjectId, ref: 'Users'},
    userId: {type: Schema.ObjectId, ref: 'Users'},
    timestamp: {type: Date, default: Date.now},
    comment: {type: String}
});

var Bans = mongoose.model('Bans', schema);

module.exports = Bans;
