var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    banId: {type: Schema.ObjectId, ref: 'Bans'},
    timestamp: {type: Date, default: Date.now},
    comment: {type: String},
    response: {type: String}
});

var BanAppeals = mongoose.model('BanAppeals', schema);

module.exports = BanAppeals;
