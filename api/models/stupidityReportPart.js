var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var _ = require('lodash');

var schema = new Schema({
    isPro: {type: Boolean, default: true},
    comment: {type: String, required: true},
    submitterId: {type: Schema.ObjectId, ref: 'Users', required: true},
    stupidityId: {type: Schema.ObjectId, ref: 'Stupidities', required: true}
}, {
    timestamps: true   
});

var ReportPart = mongoose.model('ReportsParts', schema);

module.exports = ReportPart;

