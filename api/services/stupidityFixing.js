var validators = require( '../helpers/validators' );
var errors = require( '../helpers/errors' );
var files = require( '../helpers/files' );
var fs = require( 'fs' );
var _ = require('lodash');

module.exports = function(Fix, FixVote, Stupidity) {
    return {
        find: function(params) {
	    return new Promise((resolve, reject) => {
		if(!params.user || params.user.isBanned) {
		    reject(errors[403]);
		}

		FixVote
		    .find({submitterId: params.user.id})
		    .select('stupidityFixId -_id')
		.then(votedIds => {
		    votedIds = _.map(votedIds, vote => {
			return vote.stupidityFixId;
		    });
		    return Fix.find({
			_id: {$nin: votedIds},
			submitterId: {$ne: params.user.id}
		    });
		}).then(fixes => {
		    if(fixes.length == 0){
			resolve([]);
		    } else {
			resolve([_.sample(fixes)]);
		    }
		}).catch(error => {
		    debugger;
		    reject(errors[500]);
		});
	    });
        },
	// internal use only
        get: function(id, params) {
	    return new Promise((resolve, reject) => {
		if(validators.objectId(id)) {
		    Fix.findById(id).then(fix => {
			if(!fix) {
			    reject(errors[404]);
			} else {
			    resolve(fix);
			}
		    }).catch(error => {
			reject(errors[500]);
		    });
		} else {
		    reject(errors[404]);
		}
	    });
        },
        create: function(data, params) {
	    return new Promise((resolve, reject) => {
		if(!params.user || params.user.isBanned) {
		    reject(errors[403]);
		}

		data.submitterId = params.user.id;
		data.stupidityId = params.stupidityId;

		Stupidity.count({_id: data.stupidityId}).then(count => {
		    // stupidity does not exist
		    if(count == 0) {
			reject(errors[404]);
		    }

		    return Fix.count({
			stupidityId: data.stupidityId,
			submitterId: data.submitterId
		    });
		}).then(count => {
		    // fix was already submitted by this user
		    if(count > 0) {
			reject(errors[401]);
		    }

		    return Fix.count({
			stupidityId: params.stupidityId,
			isApproved: true
		    });
		}).then(count => {
		    // stupidity has the approved fix
		    if(count > 0) {
			reject(errors[401]);
		    }

		    return files.saveImageFromBase64(data.image);
		}).then(sha => {
		    data.image = sha;

		    var newFix = new Fix(data);

		    newFix.save().then(fix => {
			resolve(fix);
		    }).catch(error => {
			// TODO: log error
			reject(errors[500]);
		    });
		}).catch(errorMessage => {
		    var error = errors[400];
		    error.message = errorMessage;
		    reject(error);
		});
	    });
        }
    };
};
