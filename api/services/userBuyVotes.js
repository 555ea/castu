var errors = require('../helpers/errors');
var files = require('../helpers/files');
var fs = require('fs');

module.exports = function(User) {
    return {
        get: function(voteCount, params) {
	    return new Promise((resolve, reject) => {
		User.find(params.payload.id).then(user => {
		    user.addVotes(voteCount);
		    return user.save();
		}).then(resolve).catch(reject);
	    });
	}
    };
};
