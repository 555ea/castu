var validators = require('../helpers/validators');
var errors = require('../helpers/errors');
var files = require('../helpers/files');
var fs = require('fs');

module.exports = function(Stupidity, StupidityVote, stupiditiesService) {
    return {
        find: function(params) {
	    return new Promise((resolve, reject) => {
		stupiditiesService.get(params.stupidityId, {}).then(stupidity => {
		    resolve(stupidity.voteCount);
		}).catch(reject); // same errors as stupiditiesService
	    });
        },
        get: function(voteCount, params) {
	    return new Promise((resolve, reject) => {
		if(voteCount <= 0) {
		    reject(errors[400]);
		} if(params.user && !params.user.isBanned) {
		    if(voteCount == 1 || (params.user.votesLeft >= voteCount)) {
			stupiditiesService.get(params.stupidityId, {}).then(stupidity => {
			    return StupidityVote.createOrUpdate(
				stupidity,
				params.user,
				voteCount
			    );
			}).then(resolve).catch(reject); // same errors as stupiditiesService
		    } else {
			// not enough votes
			reject(errors[400]);
		    }
		} else {
		    reject(errors[403]);
		}
	    });
        }
    };
};
