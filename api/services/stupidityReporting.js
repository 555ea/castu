var validators = require( '../helpers/validators' );
var errors = require( '../helpers/errors' );
var files = require( '../helpers/files' );
var fs = require( 'fs' );
var _ = require('lodash');

module.exports = function(Stupidity, Report, ReportPart) {
    return {
        find: function(params) {
	    return new Promise((resolve, reject) => {
		if(!params.user || params.user.isBanned) {
		    reject(errors[403]);
		}

		ReportPart
		    .find({submitterId: params.user.id})
		    .select('stupidityId -_id')
		.then(votedIds => {
		    votedIds = _.map(votedIds, vote => {
			return vote.stupitityId;
		    });

		    return Report.find({
			stupitityId: {$nin: votedIds},
			$and: [
			    {isApproved: false},
			    {isRejected: false}
			]
		    });
		}).then(reports => {
		    if(reports.length == 0) {
			resolve([]);
		    } else {
			var report = _.sample(reports);
			// populate stupidity so the client won't have to do multiple requests
			return Report.populate(report, {path: 'stupidityId'});
		    }
		}).then(report => {
		    ReportPart.find({stupidityId: report.stupidityId}).then(reports => {
			resolve([{
			    report: report,
			    parts: reports
			}]);
		    });
		}).catch(error => {
		    reject(errors[500]);
		});
	    });
        },
        create: function(data, params) {
	    return new Promise((resolve, reject) => {
		if(!params.user || params.user.isBanned) {
		    reject(errors[403]);
		}

		if(!data.comment) {
		    reject(errors[400]);
		}

		Stupidity.findOne({_id: params.stupidityId}).then(stupidity => {
		    if(!stupidity) {
			// stupidity does not exist 
			reject(errors[404]);
		    } else if(stupidity.isHidden) {
			// stupidity is removed already
			reject(errors[401]);
		    }

		    return Report.findOne({
			stupidityId: params.stupidityId
		    });
		}).then(report => {
		    if(report && report.hasVerdict) {
			// stupidity has an accepted report
			reject(errors[401]);
		    }

		    return ReportPart.count({
			submitterId: params.user.id,
			stupidityId: params.stupidityId
		    });
		}).then(count => {
		    if(count != 0) {
			// trying to report twice
			reject(errors[401]);
		    }

		    var newReportPart = new ReportPart({
			isPro: true,
			comment: data.comment,
			submitterId: params.user.id,
			stupidityId: params.stupidityId
		    });

		    return newReportPart.save();
		}).then(reportPart => {
		    return Report.addReportPart(
			reportPart,
			params.user.credibility.value
		    );
		}).then(resolve).catch(errors => {
		    // TODO: log errors
		    reject(errors[500]);
		});
	    });
        }
    };
};
