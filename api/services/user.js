var errors = require('../helpers/errors');
var files = require('../helpers/files');
var fs = require('fs');

module.exports = function(Stupidity, Fix, Report, ReportPart, User) {
    return {
        find: function(params) {
	    return new Promise((resolve, reject) => {
		var userId;
		var foundUser;

		if(params.user) {
		    userId = params.user.id;
		} else if(params.payload) {
		    userId = params.payload.id;
		} else {
		    reject(errors[403]);
		}

		User.findOne({_id: userId}).then(user => {
		    if(!user) {
			reject(errors[404]);
		    }

		    foundUser = user;

		    var userFields = [
			Stupidity.find({submitterId: userId}),
			Fix.find({submitterId: userId}),
			ReportPart.find({submitterId: userId})
		    ];

		    return Promise.all(userFields);
		}).then(userData => {
		    foundUser.stupidities = userData[0];
		    foundUser.fixes = userData[1];
		    foundUser.reports = userData[2];

		    resolve(foundUser);
		}).catch(reject);
	    });
	},
	get: function(id, params) {
	    var service = this;
	    return new Promise((resolve, reject) => {
		// admin only
		if(params.user && params.user.isAdmin) {
		    service.find({payload: {id: id}}).then(user => {
			if(user) {
			    resolve(user);
			} else {
			    reject(errors[404]);
			}
		    }).catch(reject);
		} else {
		    reject(errors[403]);
		}
	    });
	},
	remove: function(id, params) {
	    var service = this;
	    return new Promise((resolve, reject) => {
		// admin only
		if(params.user && params.user.isAdmin) {
		    service.find({payload: {id: id}}).then(user => {
			if(params.action === 'ban') {
			    user.isBanned = true;
			} else if(params.action === 'unban') {
			    user.isBanned = false;
			} else {
			    reject(errors[403]);
			}

			return user.save();
		    }).then(resolve).catch(reject);
		} else {
		    reject(errors[403]);
		}
	    });
	}
    };
};
