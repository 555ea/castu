var validators = require('../helpers/validators');
var errors = require('../helpers/errors');
var files = require('../helpers/files');
var fs = require('fs');
var _ = require('lodash');

module.exports = function(Stupidity) {
    return {
        find: function(params) {
            return Stupidity.find({isHidden: false}).select('-submitterId');
        },
        get: function(id, params) {
            return new Promise((resolve, reject) => {
                if(validators.objectId(id)) {
                    Stupidity.findById(id).then(stupidity => {
                        if(stupidity) {
                            resolve(stupidity);
                        } else {
                            reject(errors[404]);
                        }
                    });
                } else {
                    reject(errors[404]);
                }
            });
        },
        create: function(data, params) {
            return new Promise((resolve, reject) => {
		if(!params.user || params.user.isBanned) {
		    reject(errors[403]);
		}

                files.saveImageFromBase64(data.image).then(sha => {
		    data.submitterId = params.user.id;

                    var newStupidity = new Stupidity(data);

                    newStupidity.image = sha;

                    newStupidity.save().then(stupidity => {
                        // TODO: notify observers
                        resolve(stupidity);
                    }).catch(error => {
                        // TODO: log error
                        reject(errors[400]);
                    });
                }).catch(errorMessage => {
		    var error = _.clone(errors[400]);
		    error.message = errorMessage;
                    reject(error);
                });
            });
        },
        update: function(id, data, params) {
	    var service = this;
            return new Promise((resolve, reject) => {
		if(!params.user || !params.user.isAdmin) {
		    reject(errors[403]);
		}

		service.get(id).then(stupidity => {
		    var updateableFields = ['comment', 'severity', 'category', 'location'];
		    
		    _.forEach(updateableFields, field => {
			if(field in data) {
			    stupidity[field] = data[field];
			}
		    });
		    
		    return stupidity.save();
		}).then(resolve).catch(reject);
	    });
	},
        remove: function(id, params) {
	    var service = this;
            return new Promise((resolve, reject) => {
		if(!params.user || params.user.isBanned) {
		    reject(errors[403]);
		}

		service.get(id).then(stupidity => {
		    var now = new Date();
		    var updateTime = stupidity.createdAt;

		    updateTime.setHours(updateTime.getHours() + 1);

		    var notTimedOut = updateTime > now;
		    var bySameUser = stupidity.submitterId == params.user.id;
		    
		    if((notTimedOut && bySameUser) || params.user.isAdmin) {
			stupidity.isHidden = true;
			stupidity.save().then(resolve).catch(reject);
		    } else {
			reject(errors[401]);
		    }
		});
	    });
	}
    };
};
