var validators = require('../helpers/validators');
var errors = require('../helpers/errors');
var files = require('../helpers/files');
var fs = require('fs');

// you can only vote, for the given report, and only once
module.exports = function(Stupidity, Report, ReportPart) {
    return {
        create: function(data, params) {
	    return new Promise((resolve, reject) => {
		if(!params.user || params.user.isBanned) {
		    reject(errors[403]);
		}

		if(!data.comment) {
		    reject(errors[400]);
		}

		var isPro = false;

		if(data.vote === "pro") {
		    isPro = true;
		} else if(data.vote === "contra") {
		    isPro = false;
		} else {
		    reject(errors[400]);
		}

		Stupidity.findOne({_id: params.stupidityId}).then(stupidity => {
		    if(!stupidity) {
			// stupidity does not exist 
			reject(errors[404]);
		    } else if(!stupidity.isHidden) {
			// report has not been accepted yet
			reject(errors[401]);
		    }

		    return Report.findOne({
			stupidityId: params.stupidityId
		    });
		}).then(report => {
		    if(report && report.hasVerdict) {
			// stupidity has an accepted report
			reject(errors[401]);
		    }

		    return ReportPart.count({
			submitterId: params.user.id,
			stupidityId: params.stupidityId
		    });
		}).then(count => {
		    if(count != 0) {
			// trying to report twice
			reject(errors[401]);
		    }

		    var newReportPart = new ReportPart({
			isPro: isPro,
			comment: data.comment,
			submitterId: params.user.id,
			stupidityId: params.stupidityId
		    });

		    return newReportPart.save();
		}).then(reportPart => {
		    return Report.addReportPart(
			reportPart,
			params.user.credibility.value
		    );
		}).then(resolve).catch(errors => {
		    // TODO: log errors
		    reject(errors[500]);
		});
	    });
        }
    };
};
