var validators = require('../helpers/validators');
var errors = require('../helpers/errors');
var files = require('../helpers/files');
var fs = require('fs');

// you can only vote, for the given fix, and only once
module.exports = function(Fix, FixVote, fixService) {
    return {
        get: function(acceptance, params) {
	    return new Promise((resolve, reject) => {
		 if(params.user && !params.user.isBanned) {
		    if(acceptance < 0 || acceptance >= 10) {
			reject(errors[400]);
		    } else {
			Fix.count({
			    stupidityId: params.stupidityId,
			    isApproved: true
			}).then(count => {
			    if(count != 0) {
				// stupidity has an accepted fix
				reject(errors[401]);
			    }

			    return FixVote.count({
				submitterId: params.user.id,
				stupidityFixId: params.fixId
			    });
			}).then(count => {
			    if(count != 0) {
				// trying to vote twice
				reject(errors[401]);
			    }

			    return fixService.get(params.fixId);
			}).then(fix => {
			    if(fix.submitterId == params.user.id) {
				// trying to vote for own fix
				reject(errors[401]);
			    }

			    return FixVote.create(
				fix,
				params.user,
				acceptance,
				params.comment
			    );
			}).then(resolve).catch(errors => {
			    // TODO: log errors
			    reject(errors[500]);
			});
		    }
		} else {
		    reject(errors[403]);
		}
	    });
        }
    };
};
